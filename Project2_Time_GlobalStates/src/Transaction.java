/**
 * @author Palash Gandhi (pbg4930)
 * @date 25/2/17 6:37 PM
 * Distributed_Systems Project 2 Part 1
 */
public class Transaction {

    /**
     * Main class used to start the process at a node.
     * Run instructions: java Transaction <port_for_this_node> <hostname1>
     *     <port_for_hostname1> <hostname2> <port_for_hostname2>
     * */

    int port;
    int[] connect_hosts;
    String[] hosts;

    Transaction(int port, String[] hosts, int[] connect_hosts){
        /*
        * Constructor for this class that start the 2 threads viz.
        * TransactionHandler and TransactionCommunicator.
        * */
        this.port = port;
        this.connect_hosts = connect_hosts;
        this.hosts = hosts;
        TransactionHandler transactionHandler = new TransactionHandler(port,
                hosts, connect_hosts);
        TransactionCommunicator communicator = new TransactionCommunicator
                (port, transactionHandler);
        communicator.start();
        transactionHandler.start();
    }

    public static void main(String[] args) {
        if(args.length!=5) {
            System.out.println("Invalid usage. Usage: java Transaction " +
                    "<port_for_this_node> <hostname1> \n" +
                    "<port_for_hostname1> <hostname2> " +
                    "<port_for_hostname2>");
        }
        int port = Integer.parseInt(args[0]);
        String[] hosts = {args[1], args[3]};
        int[] connect_ports = {Integer.parseInt(args[2]), Integer.parseInt
                (args[4])};
        Transaction transaction = new Transaction(port, hosts, connect_ports);
    }
}
