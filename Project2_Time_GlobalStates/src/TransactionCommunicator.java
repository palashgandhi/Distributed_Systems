import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;

/**
 * @author Palash Gandhi (pbg4930)
 * @date 25/2/17 4:27 PM
 * Distributed_Systems Project 2 Part 1
 */
public class TransactionCommunicator extends Thread {

    /**
     * Class that is used to start a thread which will listen for incoming
     * communication from nodes while the handler is busy.
     * */

    ServerSocket server_socket;
    int port;
    TransactionHandler transactionHandler;

    TransactionCommunicator(int port, TransactionHandler transactionHandler){
        this.port = port;
        this.transactionHandler = transactionHandler;
    }

    public void run(){
        establish_connections();
        listen();
    }

    public void establish_connections(){
        /*
        * Start a server socket to listen to incoming communication.
        * */
        try {
            this.server_socket = new ServerSocket(this.port);
            System.out.println("Server socket listening at port: "+this.port);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void listen(){
        /*
        * Wait for incoming communication and pass it on to the handler as
        * required.
        * */
        while(true){
            try {
                Socket client = this.server_socket.accept();
                InputStream in = client.getInputStream();
                OutputStream os = client.getOutputStream();
                byte[] message = new byte[1024];
                in.read(message);
                ByteArrayInputStream bin = new ByteArrayInputStream(message);
                DataInputStream din = new DataInputStream(bin);
                int choice_of_transaction = din.readInt();
                int amount = din.readInt();
                HashMap<String, Integer> vector_rec = new HashMap<>();
                for(int count = 0 ; count < 3 ; count++){
                    int hostname_len = din.readInt();
                    byte[] hostname_arr = new byte[hostname_len];
                    din.read(hostname_arr);
                    String hostname = new String(hostname_arr);
                    int vector_val = din.readInt();
                    vector_rec.put(hostname, vector_val);
                }
                int success = this.transactionHandler.process_message
                        (choice_of_transaction, amount, vector_rec);
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                DataOutputStream dos = new DataOutputStream(bos);
                dos.writeInt(success);
                os.write(bos.toByteArray());
                os.flush();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
