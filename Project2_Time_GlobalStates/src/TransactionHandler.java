import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Random;

/**
 * @author Palash Gandhi (pbg4930)
 * @date 25/2/17 3:17 PM
 * Distributed_Systems Project 2 Part 1
 */
public class TransactionHandler extends Thread {

    /**
     * This class is used to handle transactions (withdraw, deposit and send)
     * and to maintain the vector clock at this node. Withdraw and deposit are
     * local events which means that only the current node balance and
     * vector* clocks are affected.
     */

    int port;
    String[] hosts;
    int[] connect_hosts;
    HashMap<Integer, String> possible_transactions = new HashMap<>();
    ServerSocket serverSocket;
    int balance = 1000;
    HashMap<String, Integer> vector_clock = new HashMap<>();
    String hostname;

    TransactionHandler(int port, String[] hosts, int[] connect_hosts) {
        /*
         * Constructor for class TransactionHandler.
         * @param port The port at which this node will communicate.
         * @param hosts Array of hosts to communicate with
         * @param connect_hosts Array of ports to connect to the hosts.
         * */
        this.possible_transactions.put(0, "withdraw");
        this.possible_transactions.put(1, "deposit");
        this.possible_transactions.put(2, "send");
        try {
            this.hostname = InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        vector_clock.put(hostname + ".cs.rit.edu", 0);
        for (int count = 0; count < hosts.length; count++) {
            vector_clock.put(hosts[count], 0);
        }
        this.hosts = hosts;
        this.port = port;
        this.connect_hosts = connect_hosts;
    }

    public void run() {
        /*
        * Start the thread to perform transactions after waiting for 5
        * secconds. (Manual timeout to wait for other hosts to start
        * listening only at the start of the program).
        * */
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        perform_transactions();
    }

    private void perform_transactions() {
        /*
        * Continuously perform different transactions at intervals of 5
        * seconds until the user stops execution.
        * */
        while (true) {
            Random rand = new Random();

            int choice_of_transaction = rand.nextInt(3);
            int amount = rand.nextInt(100);
            if (choice_of_transaction != 2) {
                System.out.println("\n\nTransaction " + this
                        .possible_transactions.get(choice_of_transaction) + "" +
                        " " +
                        ":" + amount);
                print_balance();
                vector_clock.put(hostname + ".cs.rit.edu", vector_clock.get
                        (hostname + ".cs.rit.edu") + 1);
                if (choice_of_transaction == 0) {
                    balance -= amount;
                } else if (choice_of_transaction == 1) {
                    balance += amount;
                }
                print_balance();
            } else {
                int choice_of_connect = rand.nextInt(connect_hosts.length);
                String hostname_to_connect = this.hosts[choice_of_connect];
                int port_to_connect = this.connect_hosts[choice_of_connect];
                System.out.println("\n\nConnecting to " + hostname_to_connect +
                        ":" + port_to_connect + " for transaction " + this
                        .possible_transactions.get(choice_of_transaction)
                        + " $" + amount);
                send_message(hostname_to_connect, port_to_connect,
                        choice_of_transaction, amount);
            }
            try {
                Thread.sleep(20000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void send_message(String host, int port, int choice_of_transaction,
                              int amount) {
        /*
        * Function to send a message to a given host:port combination about a
         * transaction. This is invoked only when the randomly chosen choice of
         * transaction is to send money.
         *
        * */
        vector_clock.put(hostname + ".cs.rit.edu", vector_clock.get(hostname
                + ".cs.rit.edu") + 1);
        if (choice_of_transaction == 1 || choice_of_transaction == 2) {
            if ((this.balance - amount) < 0) {
                System.out.println("Transaction will not take place. Too less" +
                        " balance at this node. Balance=" + this.balance);
                amount = 0;
                choice_of_transaction = -1;
            }
        }
        print_balance();
        try {
            Socket sock = new Socket(host, port);
            OutputStream os = sock.getOutputStream();
            InputStream in = sock.getInputStream();
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            DataOutputStream dos = new DataOutputStream(bos);
            dos.writeInt(choice_of_transaction);
            dos.writeInt(amount);
            for (String host_name : vector_clock.keySet()) {
                if (host_name.equals(hostname + ".cs.rit.edu")) {
                    dos.writeInt(hostname.length());
                    dos.write(hostname.getBytes());
                    dos.writeInt(vector_clock.get(hostname + ".cs.rit.edu"));
                } else {
                    dos.writeInt(host_name.length());
                    dos.write(host_name.getBytes());
                    dos.writeInt(vector_clock.get(host_name));
                }
            }
            os.write(bos.toByteArray());
            os.flush();
            int success = process_response(in);
            if (success == 1) {
                System.out.println("Transaction successful.");
                if (choice_of_transaction == 1 || choice_of_transaction == 2) {
                    balance -= amount;
                } else if (choice_of_transaction == 0) {
                    balance += amount;
                }
            } else {
                System.out.println("Transaction failed.");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        print_balance();
    }

    private int process_response(InputStream in) {
        /*
        * Process a response that is received at this node after it has sent
        * a request for transaction to other nodes.
        * @param in InputStream for the socket at which message is received.
        * */
        byte[] response = new byte[512];
        int success = 0;
        try {
            in.read(response);
            ByteArrayInputStream bin = new ByteArrayInputStream(response);
            DataInputStream din = new DataInputStream(bin);
            success = din.readInt();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return success;
    }

    public int process_message(int choice_of_transaction, int amount,
                               HashMap<String, Integer> vector_rec) {
        /*
        * Process incoming transaction messages from other nodes.
        * @param choice_of_transaction One out of (withdraw, deposit or send)
        * @param amount Amount that the node has sent
        * @param vector_rec The vector clock of the requesting node.
        * */
        vector_clock.put(hostname + ".cs.rit.edu", vector_clock.get(hostname
                + ".cs.rit.edu") + 1);
        System.out.println("\n\n");
        print_balance();
        System.out.println("Received vector clock: " + vector_rec.toString());
        for (String host_rec : vector_rec.keySet()) {
            for (String host : vector_clock.keySet()) {
                if (host.contains(host_rec) || host_rec.contains(host)) {
                    if (vector_clock.get(host) < vector_rec.get(host_rec)) {
                        vector_clock.put(host, vector_rec.get(host_rec));
                    }
                }
                if (host.equals(hostname + ".cs.rit.edu") && host.contains
                        (hostname)) {
                    if (vector_clock.get(hostname + ".cs.rit.edu") <
                            vector_rec.get(host_rec)) {
                        vector_clock.put(hostname + ".cs.rit.edu", vector_rec
                                .get(host_rec));
                    }
                }
            }
        }
        System.out.println("Received message: " + this.possible_transactions
                .get(choice_of_transaction) + " $" + amount);
        int success = 0;
        switch (choice_of_transaction) {
            case 0:
                if ((balance - amount) > 0) {
                    balance -= amount;
                    success = 1;
                }
                break;
            case 1:
                balance += amount;
                success = 1;
                break;
            case 2:
                balance += amount;
                success = 1;
                break;
        }
        print_balance();
        return success;
    }

    public void print_balance() {
        /*
        * Helper function to print balance.
        * */
        System.out.println("=================================================");
        System.out.println("Balance: " + balance);
        System.out.println("Vector clock: " + vector_clock.toString());
        System.out.println("=================================================");
    }
}
