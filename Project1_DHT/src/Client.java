import java.io.*;
import java.net.Socket;
import java.util.*;

/**
 * @author Palash Gandhi (pbg4930)
 * @date 2/6/17 3:11 PM
 * Distributed_Systems
 */
public class Client {
    /**
     * This class acts as the client that communicates with the distributed
     * cluster. It performs operations like insertion and lookup.
     * */

    static String[] CLUSTER_HOSTS = {"yes.cs.rit.edu", "doors.cs" +
            ".rit.edu",
            "comet.cs.rit.edu", "rush.cs.rit.edu", "queen.cs.rit.edu",
            "queeg.cs.rit.edu", "gorgon.cs.rit.edu"};
    static List<String> HOSTS = new ArrayList<String>();

    static String[] FILE_NAMES = {"portal.txt", "purple_haze.txt",
            "john_lenon_rock.txt", "jim_morrison.txt", "roger_waters.txt",
            "gorgon.txt", "mick_jagger.txt"};
    static int PORT = 5444;
    static Hashtable<Integer, String> local_hashtable = new Hashtable<>();

    static void insert() {
        /**
         * Method that inserts all the files into the different hosts based
         * on the calculated hash value.
         * */
        for(int counter = 0 ; counter < FILE_NAMES.length ;
            counter++){
            int hash_index = hash(FILE_NAMES[counter], 0 ,0)%7;
            System.out.println("Hash index for file "+FILE_NAMES[counter]+" " +
                    "with i=0 and j=0: " + hash_index);
            System.out.println("Inserting file "+FILE_NAMES[counter]+" in " +
                    CLUSTER_HOSTS[hash_index]+"\n");
            try {
                Socket socket = new Socket(CLUSTER_HOSTS[hash_index], PORT);
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                DataOutputStream ds = new DataOutputStream(bos);
                ds.writeInt(1);
                ds.writeInt(FILE_NAMES[counter].length());
                ds.write(FILE_NAMES[counter].getBytes());
                byte[] to_send = bos.toByteArray();
                OutputStream out = socket.getOutputStream();
                out.write(to_send);
                out.flush();
            }catch (Exception e){
                e.printStackTrace();
                System.out.println(e);
                System.exit(2);
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    static void get_leaves(String file){
        /**
         * Helper function to perform lookup. It fetches all the leaves of
         * the binary tree for a given file name so that the client can make
         * requests to these leaves.
         * @param file: Name of the file.
         * */
        int hash_index = hash(file, 0, 0)%7;
        String root = CLUSTER_HOSTS[hash_index];
        try {
            Socket sock = new Socket(root, PORT);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            DataOutputStream dos = new DataOutputStream(bos);
            dos.writeInt(3);
            dos.writeInt(file.length());
            dos.write(file.getBytes());
            OutputStream os = sock.getOutputStream();
            os.write(bos.toByteArray());
            os.flush();
            byte[] rec = new byte[2048];
            InputStream in = sock.getInputStream();
            in.read(rec);
            ByteArrayInputStream bin = new ByteArrayInputStream(rec);
            DataInputStream din = new DataInputStream(bin);
            for(int count = 0 ; count < 4 ; count++){
                int hash_val_rec = din.readInt();
                if(hash_val_rec != 0) {
                    int hostname_len = din.readInt();
                    byte[] host_arr = new byte[hostname_len];
                    din.read(host_arr);
                    String hostname = new String(host_arr);
                    local_hashtable.put(hash_val_rec, hostname);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static void lookup(String file_name){
        /**
         * Method that looks up the file location and requests it from the
         * distributed cluster.
         * @param file_name: The name of the file.
         * */
        get_leaves(file_name);
        System.out.println("=================================================");
        System.out.println("Local Hashtable");
        System.out.println(local_hashtable.toString());
        System.out.println("=================================================");
        int i = 2;
        Random rand = new Random();
        int j = rand.nextInt(4);
        int hash_val = hash(file_name, i, j);
        if(local_hashtable.containsKey(hash_val)) {
            String leaf = local_hashtable.get(hash_val);
            System.out.println("Sending search request to randomly selected " +
                    "node: " + leaf);
            try {
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                DataOutputStream ds = new DataOutputStream(bos);
                ds.writeInt(5);
                ds.writeInt(file_name.length());
                ds.write(file_name.getBytes());
                ds.writeInt(i);
                ds.writeInt(j);
                byte[] to_send = bos.toByteArray();
                Socket socket = new Socket(leaf, PORT);
                OutputStream out = socket.getOutputStream();
                InputStream in = socket.getInputStream();
                out.write(to_send);
                out.flush();
                process_response(in);
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println(e);
            }
        }
        else{
            System.out.println("File not found.\n");
        }

    }

    static void process_response(InputStream in){
        /**
         * Processes the response that the cluster sends back after the
         * client issues a search request.
         * @param in: InputStream object to read the response from the node.
         * */
        System.out.println("Waiting for response...");
        byte[] response = new byte[2048];
        try {
            in.read(response);
        } catch (IOException e) {
            e.printStackTrace();
        }
        ByteArrayInputStream bin = new ByteArrayInputStream(response);
        DataInputStream din = new DataInputStream(bin);
        try {
            int code = din.readInt();
            int file_len = din.readInt();
            byte[] filename_arr = new byte[file_len];
            din.read(filename_arr);
            String filename = new String(filename_arr);
            int found = din.readInt();
            if(found==1){
                System.out.println("\nFile found.");
            }
            int count = din.readInt();
            System.out.println("Total number of hops: "+(count-1));
            System.out.println("============================================");
            for(int count_hops = 0; count_hops<count-1; count_hops++){
                int host_len = din.readInt();
                byte[] host_arr = new byte[host_len];
                din.read(host_arr);
                System.out.print(new String(host_arr));
                if(count_hops < count-1) {
                    System.out.print(" => ");
                }
            }
            System.out.println
                    ("\n============================================");
            System.out.println("\n\n");
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    static int hash(String str, int i, int j) {
        /**
         * Common hash function also available at the nodes.
         * @param str: String of which hash value is required.
         * @param i: The level in the binary tree.
         * @param j: e.g jth node at level i.
         * */
        return Math.abs((str+i+j).hashCode());
    }

    public static void main(String[] args) {
        while (true) {
            System.out.println("Select one of the following operations: ");
            System.out.println("1. Insertion");
            System.out.println("2. Lookup");
            Scanner sc = new Scanner(System.in);
            int selection = sc.nextInt();
            if (selection == 1) {
                insert();
            } else if (selection == 2) {
                System.out.println("Enter the name of file to search: ");
                String file_name = sc.next();
                System.out.println("Enter the number of times to search: ");
                int repeat = sc.nextInt();
                for(int count = 0 ; count < repeat ; count++) {
                    lookup(file_name);
                }
            }
            else{
                System.out.println("Invalid operation. Try again...\n");
            }
        }
    }
}
