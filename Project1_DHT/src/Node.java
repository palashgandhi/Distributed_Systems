import java.io.*;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;

/**
 * @author Palash Gandhi (pbg4930)
 * @date 13/2/17 7:57 PM
 * Distributed_Systems Project 1
 */
public class Node {
    /**
     * This class is used as a Node in a distributed system. A Node instance
     * is responsible for all operations like creating a file, building a
     * logical tree for a file, determining the leaves for a built tree,
     */
    static String MY_HOSTNAME;
    static ServerSocket server_socket;
    static int PORT = 5444;
    static String[] CLUSTER_HOSTS = {"yes.cs.rit.edu", "doors.cs.rit.edu",
            "comet.cs.rit.edu", "rush.cs.rit.edu", "queen.cs.rit.edu",
            "queeg.cs.rit.edu", "gorgon.cs.rit.edu"};
    static String current_hostname = "";
    static HashMap<String, Integer> file_counts = new HashMap<>();
    static HashMap<String, ArrayList<String>> file_replication = new HashMap<>();
    private static Hashtable<Integer, String> local_hashtable = new
            Hashtable<>();

    Node() {
        try {
            MY_HOSTNAME = InetAddress.getLocalHost().getHostName();
            System.out.println("My HostName: " + MY_HOSTNAME);
            for (int count = 0; count < CLUSTER_HOSTS.length; count++) {
                if (CLUSTER_HOSTS[count].contains(MY_HOSTNAME)) {
                    current_hostname = CLUSTER_HOSTS[count];
                }
            }
            server_socket = new ServerSocket(PORT);
            listen();
        } catch (UnknownHostException ue) {
            ue.printStackTrace();
            System.out.println(ue);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e);
        }
    }

    static void listen() {
        try {
            System.out.println("1. Listening at port " + PORT);
            while (true) {
                System.out.println("\nWaiting for activity...");
                Socket client_socket = server_socket.accept();
                byte[] message = new byte[2048];
                InputStream in = client_socket.getInputStream();
                OutputStream os_client = client_socket.getOutputStream();
                process_message(in, os_client, message);
                System.out.println("Local Hashtable: "+local_hashtable.toString());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static void process_message(InputStream in, OutputStream os, byte[]
            message){
        try {
            in.read(message);
            ByteArrayInputStream bis = new ByteArrayInputStream(message);
            DataInputStream din = new DataInputStream(bis);
            int message_code = din.readInt();
            int filename_len = din.readInt();
            byte[] file_name_arr = new byte[filename_len];
            din.read(file_name_arr);
            String file_name = new String(file_name_arr);
            System.out.println("\nCode received: "+message_code);
            if (message_code == 1) {
                create_file(file_name, "");
                build_tree(file_name, 0, 0, new ArrayList<>());
            }
            else if(message_code == 2){
                int i = din.readInt();
                int j = din.readInt();
                int parent_len = din.readInt();
                byte[] parent_arr = new byte[parent_len];
                din.read(parent_arr);
                String parent_hostname = new String(parent_arr);
                add_parent_to_hashtable(parent_hostname, file_name, i, j);
                ArrayList<String> unavailable = read_unavailable(din);
                if(i<2) {
                    unavailable = build_tree(file_name, i, j,
                            unavailable);
                }
                byte[] response = respond_back(unavailable);
                os.write(response);
                os.flush();
            }
            else if(message_code == 3){
                HashMap<Integer, String> leaves;
                leaves = get_leaves(file_name);
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                DataOutputStream dos = new DataOutputStream(bos);
                if(leaves.size() > 0) {
                    for (int hash_val : leaves.keySet()) {
                        dos.writeInt(hash_val);
                        String leaf = leaves.get(hash_val);
                        dos.writeInt(leaf.length());
                        dos.write(leaf.getBytes());
                    }
                }
                else{
                    dos.writeInt(0);
                }
                os.write(bos.toByteArray());
                os.flush();
            }
            else if(message_code == 4){
                int j_begin = din.readInt();
                int i = 2;
                if(j_begin==1){
                    j_begin += 1;
                }
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                DataOutputStream dos = new DataOutputStream(bos);
                for(int j=j_begin; j<j_begin+2; j++){
                    int hash_val = hash(file_name, i, j);
                    String hostname = local_hashtable.get(hash_val);
                    dos.writeInt(hash_val);
                    dos.writeInt(hostname.length());
                    dos.write(hostname.getBytes());
                }
                os.write(bos.toByteArray());
                os.flush();
            }
            else if(message_code == 5){
                System.out.println("Received search request from a client for" +
                        " file: "+file_name);
                String current = get_current_host_name();
                File f = new File(current+"/"+file_name);
                byte[] response;
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                DataOutputStream dos = new DataOutputStream(bos);
                if(f.exists() && !f.isDirectory()){
                    System.out.println("File found here. "+file_name);
                    update_search_count(file_name, 2);
                    dos.writeInt(8);
                    dos.writeInt(filename_len);
                    dos.write(file_name_arr);
                    dos.writeInt(1);
                    dos.writeInt(1);
                    dos.writeInt(current.length());
                    dos.write(current.getBytes());
                }else {
                    int i = din.readInt();
                    int j = din.readInt();
                    int j_parent = 0;
                    if (j > 1) {
                        j_parent = 1;
                    }
                    int i_parent = i - 1;
                    int hash_val = hash(file_name, i_parent, j_parent);
                    String parent = local_hashtable.get(hash_val);
                    System.out.println("Requesting parent: " + parent);
                    byte[] response_from_parent = request_file(parent,
                            file_name, false);
                    ByteArrayInputStream bis_test = new ByteArrayInputStream
                            (response_from_parent);
                    DataInputStream dis = new DataInputStream(bis_test);
                    int code = dis.readInt();
                    int file_le = dis.readInt();
                    byte[] file_arr = new byte[file_le];
                    dis.read(file_arr);
                    int found1 = dis.readInt();
                    int count_hops = dis.readInt();
                    dos.writeInt(code);
                    dos.writeInt(file_le);
                    dos.write(file_arr);
                    dos.writeInt(found1);
                    dos.writeInt(count_hops+1);
                    dos.writeInt(current.length());
                    dos.write(current.getBytes());
                    for(int count = 0; count<count_hops ; count++ ){
                        int host_len = dis.readInt();
                        dos.writeInt(host_len);
                        byte[] host_arr = new byte[host_len];
                        dis.read(host_arr);
                        dos.write(host_arr);
                    }
                    dos.flush();
                }
                response = bos.toByteArray();
                os.write(response);
                os.flush();
            }
            else if(message_code == 6){
                System.out.println("Received search request from child for " +
                        ""+file_name);
                String current = get_current_host_name();
                File f = new File(current+"/"+file_name);
                byte[] response;
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                DataOutputStream dos = new DataOutputStream(bos);
                if(f.exists() && !f.isDirectory()){
                    update_search_count(file_name, 1);
                    dos.writeInt(8);
                    dos.writeInt(filename_len);
                    dos.write(file_name_arr);
                    dos.writeInt(1);
                    dos.writeInt(2);
                    dos.writeInt(current.length());
                    dos.write(current.getBytes());
                }
                else {
                    int root_hash = hash(file_name, 0, 0);
                    String root = local_hashtable.get(root_hash);
                    byte[] response_from_root = request_file(root, file_name,
                            true);
                    ByteArrayInputStream bis_test = new ByteArrayInputStream
                            (response_from_root);
                    DataInputStream dis = new DataInputStream(bis_test);
                    int code = dis.readInt();
                    int file_le = dis.readInt();
                    byte[] file_arr = new byte[file_le];
                    dis.read(file_arr);
                    int found1 = dis.readInt();
                    dos.writeInt(code);
                    dos.writeInt(file_le);
                    dos.write(file_arr);
                    dos.writeInt(found1);
                    dos.writeInt(3);
                    dos.writeInt(current.length());
                    dos.write(current.getBytes());
                    dos.writeInt(root.length());
                    dos.write(root.getBytes());
                }
                response = bos.toByteArray();
                os.write(response);
                os.flush();
            }
            else if(message_code == 7){
                System.out.println("Received search request from child for " +
                        ""+file_name);
                String host = get_current_host_name();
                File f = new File(host+"/"+file_name);
                int found = 0;
                if(f.exists() && !f.isDirectory()) {
                    System.out.println("File found: "+host+"/"+file_name);
                    update_search_count(file_name, 0);
                    found = 1;
                }
                if (found == 0) {
                    System.out.println("File not found: "+host+"/"+file_name);
                }
                byte[] response = build_response_file_found(file_name, found);
                os.write(response);
                os.flush();
            }
            else if(message_code == 9){
                System.out.println("Replication request received. ");
                int file_contents_len = din.readInt();
                byte[] file_arr = new byte[file_contents_len];
                din.read(file_arr);
                String file_contents = new String(file_arr);
                create_file(file_name, file_contents);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static byte[] build_response_file_found(String file, int found){
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        DataOutputStream dos = new DataOutputStream(bos);
        try {
            dos.writeInt(8);
            dos.writeInt(file.length());
            dos.write(file.getBytes());
            dos.writeInt(found);
            dos.flush();
        }catch (Exception e){
            e.printStackTrace();
        }
        return bos.toByteArray();
    }

    static void update_search_count(String file_name, int i){
        if(file_counts.containsKey(file_name)) {
            file_counts.put(file_name, file_counts.get(file_name) + 1);
            if (file_counts.get(file_name) >= 5 && i < 2) {
                replicate(file_name, i);
                file_counts.put(file_name, 0);
            }
        }
    }

    static void replicate(String file, int i){
        System.out.println("\nFile "+file+" searched 5 or more times. " +
                "Replicating to child....(Level "+i+" to level "+(i+1)+")\n");
        int i_target = i+1;
        for(int j = 0 ; j < 4 ; j++){
            int hash_val = hash(file, i_target , j);
            String hostname = local_hashtable.get(hash_val);
            if(!file_replication.containsKey(file)){
                file_replication.put(file, new ArrayList<String>());
            }
            if(local_hashtable.containsKey(hash_val) && (file_replication
                    .containsKey(file) && !file_replication.get(file)
                    .contains(hostname))){
                System.out.println("Replicating to "+hostname);
                copy_file_to_host(hostname, file);
                break;
            }
        }

    }

    static void copy_file_to_host(String hostname, String filename){
        String current = get_current_host_name();
        ArrayList<String> existing = file_replication.get(filename);
        existing.add(hostname);
        file_replication.put(filename, existing);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        DataOutputStream dos = new DataOutputStream(bos);
        try {
            dos.writeInt(9);
            dos.writeInt(filename.length());
            dos.write(filename.getBytes());
            String file_contents = get_file_contents(current+"/"+filename);
            dos.writeInt(file_contents.length());
            dos.write(file_contents.getBytes());
            Socket sock = new Socket(hostname, PORT);
            OutputStream os = sock.getOutputStream();
            os.write(bos.toByteArray());
            os.flush();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    static String get_file_contents(String path){
        InputStream ins = null;
        String file = "";
        try {
            ins = new FileInputStream(path);
            BufferedReader buf = new BufferedReader(new InputStreamReader(ins));
            String line = buf.readLine();
            StringBuilder sb = new StringBuilder();
            while(line!=null){
                sb.append(line).append("\n");
                line = buf.readLine();
            }
            file = sb.toString();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }

    static byte[] request_file(String parent, String file_name, boolean is_root){
        byte[] response = new byte[2048];
        try{
            Socket sock = new Socket(parent, PORT);
            OutputStream os = sock.getOutputStream();
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            DataOutputStream dos = new DataOutputStream(bos);
            if(is_root) {
                dos.writeInt(7);
            }
            else{
                dos.writeInt(6);
            }
            dos.writeInt(file_name.length());
            dos.write(file_name.getBytes());
            os.write(bos.toByteArray());
            os.flush();
            InputStream in = sock.getInputStream();
            in.read(response);
        }catch (Exception e){
            e.printStackTrace();
        }
        return response;
    }

    static HashMap<Integer, String> get_leaves(String file){
        HashMap<Integer, String> leaves = new HashMap<>();
        for(int j = 0 ; j <=1 ; j++){
            int hash_value = hash(file, 1, j);
            if(local_hashtable.containsKey(hash_value)) {
                String host = local_hashtable.get(hash_value);
                try {
                    Socket child_sock = new Socket(host, PORT);
                    request_leaves_from_children(child_sock, file, j, host);
                    HashMap<Integer, String> interim = read_leaves_from_children
                            (child_sock);
                    for (int hash_val : interim.keySet()) {
                        leaves.put(hash_val, interim.get(hash_val));
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return leaves;
    }

    static void request_leaves_from_children(Socket child_sock, String file,
                                             int j,
                                             String host){
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            DataOutputStream dos = new DataOutputStream(bos);
            dos.writeInt(4);
            dos.writeInt(file.length());
            dos.write(file.getBytes());
            dos.writeInt(j);
            OutputStream os = child_sock.getOutputStream();
            os.write(bos.toByteArray());
            os.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static HashMap<Integer, String> read_leaves_from_children(Socket sock){
        HashMap<Integer, String> leaves = new HashMap<>();
        try{
            InputStream in = sock.getInputStream();
            byte[] resp = new byte[2048];
            in.read(resp);
            ByteArrayInputStream bin = new ByteArrayInputStream(resp);
            DataInputStream din = new DataInputStream(bin);
            for(int count = 0 ; count < 2 ; count++){
                int hash_val = din.readInt();
                int host_len = din.readInt();
                byte[] host = new byte[host_len];
                din.read(host);
                String hostname = new String(host);
                leaves.put(hash_val, hostname);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return leaves;
    }

    static byte[] respond_back(ArrayList<String> unavailable){
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        DataOutputStream dos = new DataOutputStream(bos);
        try {
            dos.writeInt(unavailable.size());
            for(String host: unavailable){
                dos.writeInt(host.length());
                dos.write(host.getBytes());
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return bos.toByteArray();

    }

    static void add_parent_to_hashtable(String parent_hn, String file_name, int
            i , int j){
        int parent_index = 0;
        if(i==2){
            if(j==0 || j==1) {
                parent_index = 0;
            }
            else if(j==2 || j==3){
                parent_index = 1;
            }
        }
        System.out.println("Adding parent "+parent_hn+" with i="+(i-1)+" and " +
                "j="+parent_index);
        int hash_parent = hash(file_name, i-1, parent_index);
        add_to_hashtable(hash_parent, parent_hn);
    }

    static ArrayList<String> read_unavailable(DataInputStream di){
        ArrayList<String> unavailable = new ArrayList<>();
        try {
            int count_unavail_hosts = di.readInt();
            for(int count = 0 ; count < count_unavail_hosts ; count++){
                int host_len = di.readInt();
                byte[] host_arr = new byte[host_len];
                di.read(host_arr);
                if(host_len>0) {
                    unavailable.add(new String(host_arr));
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            System.out.println(e);
        }
        return unavailable;
    }

    static ArrayList<String> build_tree(String file_name, int i, int j,
    ArrayList<String>
            unavailable){
        if(i==1 && j==1){
            j = 2;
        }
        int count_child = j;
        String current_hostname = get_current_host_name();
        if(!unavailable.contains(current_hostname)){
            unavailable.add(current_hostname);
        }
        ArrayList<String> picked_children = pick_children(file_name, i, j, unavailable);
        System.out.println("\tPicked children: "+picked_children.toString());
        for(String child: picked_children){
            unavailable.add(child);
        }
        for(String child_hostname: picked_children){
            System.out.println("\tConnecting to "+child_hostname);
            if(!unavailable.contains(child_hostname)) {
                unavailable.add(child_hostname);
            }
            try {
                Socket child_socket = new Socket(child_hostname, PORT);
                OutputStream child_os = child_socket.getOutputStream();
                InputStream child_is = child_socket.getInputStream();
                ByteArrayOutputStream child_bos = new ByteArrayOutputStream();
                DataOutputStream child_dos = new DataOutputStream
                        (child_bos);
                child_dos.writeInt(2);
                child_dos.writeInt(file_name.length());
                child_dos.write(file_name.getBytes());
                child_dos.writeInt(i+1);
                child_dos.writeInt(count_child);
                child_dos.writeInt(current_hostname.length());
                child_dos.write(current_hostname.getBytes());
                child_dos = write_unavailable_hosts(child_dos, unavailable);
                count_child+=1;
                child_os.write(child_bos.toByteArray());
                child_os.flush();
                byte[] response_from_child = new byte[2048];
                child_is.read(response_from_child);
                ByteArrayInputStream child_bis = new ByteArrayInputStream(response_from_child);
                DataInputStream child_dis = new DataInputStream(child_bis);
                int total_unavailable = child_dis.readInt();
                for(int count = 0 ; count < total_unavailable ; count++){
                    int len = child_dis.readInt();
                    byte[] host_arr = new byte[len];
                    child_dis.read(host_arr);
                    String hostname = new String(host_arr);
                    if(!unavailable.contains(hostname)){
                        unavailable.add(hostname);
                    }
                }

            }catch (Exception e){
                e.printStackTrace();
                System.out.println(e);
            }
        }
        return unavailable;
    }

    private static DataOutputStream write_unavailable_hosts(DataOutputStream child_dos,
                                                ArrayList<String> unavailable) {
        try {
            child_dos.writeInt(child_dos.size());
            for (String hostname : unavailable) {
                child_dos.writeInt(hostname.length());
                child_dos.write(hostname.getBytes());
            }
        }catch (Exception e){
            e.printStackTrace();
            System.out.println(e);
        }
        return child_dos;
    }

    static ArrayList<String> pick_children(String file, int i, int j_begin,
                                   ArrayList<String> unavailable){
        ArrayList<String> picked_children = new ArrayList<>();
        for(int j = j_begin ; j < j_begin+2 ;
            j++){
            int hash_value = hash(file, i+1, j);
            int hash_index = hash_value % 7;
            boolean not_picked = true;
            while(not_picked) {
                String picked = CLUSTER_HOSTS[hash_index];
                if (!picked.contains(MY_HOSTNAME) &&
                        !unavailable.contains(picked) &&
                        !picked_children.contains(picked)) {
                    picked_children.add(picked);
                    add_to_hashtable(hash_value, picked);
                    not_picked = false;
                }
                else{
                    hash_index += 1;
                    if(hash_index>=CLUSTER_HOSTS.length){
                        hash_index = 0;
                    }
                }
            }
        }
        return picked_children;
    }

    static void add_to_hashtable(int hash_val, String host){
        local_hashtable.put(hash_val, host);
    }

    static String get_current_host_name(){
        String host = "";
        for(int count = 0 ; count < CLUSTER_HOSTS.length ; count++){
            if(CLUSTER_HOSTS[count].contains(MY_HOSTNAME)){
                host = CLUSTER_HOSTS[count];
            }
        }
        return host;
    }

    static void create_file(String file_name, String contents){
        try {
            file_counts.put(file_name, 0);
            String host = get_current_host_name();
            File host_directory = new File(host);
            if(!host_directory.exists() && !host_directory.isDirectory()){
                host_directory.mkdir();
            }
            File file = new File(host+"/"+file_name);
            file.createNewFile();
            try (Writer writer = new BufferedWriter(new OutputStreamWriter
                    (new FileOutputStream(host+"/"+file_name), StandardCharsets
                            .UTF_8))) {
                writer.write(contents);
            }
            catch (IOException ex) {
                // Handle me
            }
            System.out.println("New File created. "+file_name);
        }catch (Exception e){
            e.printStackTrace();
            System.out.println(e);
        }
    }

    static int hash(String str, int i, int j) {
        return Math.abs((str + i + j).hashCode());
    }


    public static void main(String[] args) {
        Node node = new Node();
    }
}
