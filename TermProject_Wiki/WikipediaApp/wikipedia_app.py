"""
__author__ = "Palash Gandhi (pbg4930@rit.edu)"
__date__ = 05/02/2017
"""
import os
import traceback
import urllib
import urllib2
import psycopg2
from flask import Flask, render_template, request, json

'''
Main application flask app to serve the wikipedia functionality requests.
'''

app = Flask(__name__)


@app.route('/')
def index():
    return ("Please go to http://yes.cs.rit.edu:55000")


@app.route('/login_validate', methods=["GET"])
def login_validate():
    """
    Validate a login.
    """
    error = None
    role = None
    users = execute_psql_query("SELECT * FROM users", get=True)
    if len(users) == 0:
        error = 'User does not exist.'
    if request.method == 'GET' and error is None:
        user = request.args.get('username')
        password = request.args.get('password')
        validated = False
        for user_details in users:
            if user_details[1] == user and user_details[2] == password:
                validated = True
                role = user_details[3]
                break
        if not validated:
            error = "Incorrect credentials."
    if error:
        return error
    return "{0}".format(role)


@app.route('/create_user', methods=["GET"])
def create_user():
    """
    Create a user.
    """
    try:
        error = None
        users = execute_psql_query("SELECT * FROM users", get=True)
        if request.method == 'GET':
            user = request.args.get('username')
            password = request.args.get('password')
            role = request.args.get('role')
            exists = False
            for user_details in users:
                if user_details[1] == user and user_details[2] == password:
                    exists = True
                    break
            if not exists:
                execute_psql_query("INSERT INTO users (username, password, "
                                   "role ) VALUES ('{0}', '{1}', '{2}')".format(
                    user, password, role))
            else:
                error = "Username taken. Please choose another."
        if error:
            return error
        print "New user created: {0}".format(user)
        users = execute_psql_query("SELECT * FROM users", get=True)
        return "1"
    except:
        print traceback.print_exc()


@app.route('/write_article', methods=["GET"])
def write_article():
    """
    Writes a new article to the psql container.
    """
    try:
        user = request.args.get('username')
        title = request.args.get('title')
        article_text = request.args.get('article_text')
        try:
            execute_psql_query("INSERT INTO articles (title, article_text, "
                               "created_by) VALUES ('{0}', '{1}', '{2}')".format(
                title, article_text, user))
        except:
            print traceback.print_exc()
            return '0'
        return '1'
    except:
        traceback.print_exc()


@app.route('/get_article_text', methods=["GET"])
def get_article_details():
    """
    Used to fetch the article text.
    """
    try:
        title = request.args.get('title')
        article_details_db = execute_psql_query("SELECT title, article_text, "
                                                "created_by FROM articles WHERE "
                                                "title='{0}'".format(title),
                                                get=True)
        article_details = dict()
        article_details["title"] = article_details_db[0][0]
        article_details["article_text"] = article_details_db[0][1]
        article_details["created_by"] = article_details_db[0][2]
        return json.dumps(article_details)
    except:
        traceback.print_exc()


@app.route('/edit_article_submit', methods=["GET"])
def edit_article_submit():
    """
    Called when a user edits an article.
    """
    try:
        print "Editing article"
        title = request.args.get('title')
        article_text = request.args.get('article_text')
        execute_psql_query("UPDATE articles SET article_text='{0}' WHERE "
                           "title='{1}'".format(article_text, title))
        return "1"
    except:
        traceback.print_exc()
        return "0"


@app.route('/replicate_data', methods=["GET"])
def replicate_data():
    """
    Called when the load balancer instructs to replicate data from the psql 
    container to another host.
    """
    try:
        replicate_to = request.args.get('replicate_to')
        print "Replicating data to {0}".format(replicate_to)
        message = dict()
        message["users"] = json.dumps(execute_psql_query("SELECT username, "
                                                    "password, "
                                              "role FROM users", get=True))
        message["articles"] = json.dumps(execute_psql_query(
            "SELECT title, article_text, created_by"
            " FROM articles", get=True))
        response = make_request_to_url(
            "http://{0}/import_data".format(replicate_to), message)
        if response == "1":
            print "Replication successful."
            return "1"
        else:
            print "Replication not successful."
            return "0"
    except:
        traceback.print_exc()


@app.route('/import_data', methods=["GET"])
def import_data():
    """
    Called when another container on another host sends data to replicate at 
    this host.
    """
    try:
        users = request.args.get('users')
        for user in json.loads(users):
            print "USER: "+str(user)
            execute_psql_query("INSERT INTO users (username, password, "
                               "role ) VALUES ('{0}', '{1}', '{2}')".format(
                user[0], user[1], user[2]))
        articles = request.args.get('articles')
        print "\n"
        print "Articles replicated: "+str(articles)
        print "\n"
        for article in json.loads(articles):
            execute_psql_query("INSERT INTO articles (title, article_text, "
                               "created_by) VALUES ('{0}', '{1}', '{2}')".format(
                article[0], article[1], article[2]))
        return "1"
    except:
        traceback.print_exc()
        return "0"


def make_request_to_url(url, message=None):
    """
    Helper function to make a request to a url.
    """
    if message:
        data = urllib.urlencode(message)
        req = urllib2.Request("{0}?{1}".format(url, data))
    else:
        req = urllib2.Request("{0}".format(url))
    response = urllib2.urlopen(req)
    return response.read()


def create_tables_psql():
    """
    Creates tables if they do not already exist.
    """
    print "Creating tables if not exist"
    commands = ("""
        CREATE TABLE IF NOT EXISTS users (
            id SERIAL PRIMARY KEY,
            username TEXT NOT NULL,
            PASSWORD TEXT NOT NULL,
            ROLE INT NOT NULL
        )
        """, """
        CREATE TABLE IF NOT EXISTS articles (title TEXT NOT NULL, article_text TEXT NOT NULL, created_by TEXT NOT NULL)
        """)
    for command in commands:
        execute_psql_query(command)


def execute_psql_query(query, get=False):
    """
    Helper function to execute PostgreSQL queries and return results if any.
    """
    try:
        conn = psycopg2.connect(
            "host='psql' dbname='wikipedia' user='docker' password='docker'")
        cur = conn.cursor()
        cur.execute(query)
        if get:
            records = cur.fetchall()
            return records
        conn.commit()
        conn.close()
    except:
        print traceback.print_exc()


if __name__ == '__main__':
    create_tables_psql()
    app.run(host="0.0.0.0", port=os.environ["port"], threaded=True)
