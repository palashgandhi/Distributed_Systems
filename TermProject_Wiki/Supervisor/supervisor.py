"""
__author__ = "Palash Gandhi (pbg4930@rit.edu)"
__date__ = 04/29/2017
"""
import os
import socket
import threading
import json
import subprocess
import urllib
import urllib2
import settings

'''
This file contains the logic for load balancer which acts as the entry point 
into the system. 
'''

socket_object = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
hostname = socket.getfqdn()


class Supervisor:
    """
    Main class for the supervisor process that is responsible for 
    """
    current_load_balancer = None

    def __init__(self):
        self._setup_psql_container()
        self.listener = SupervisorListener()
        self.start_threads()

    def create_contatiner(self):
        """
        Method that is called when the load balancer sends a message to the 
        supervisor asking it to create a new container.
        """
        port = settings.current_available_port
        container_name = hostname + "_" + str(port)
        self._run_docker_container(container_name, port,
                                   settings.wikipedia_app_container_name)
        while True:
            s = subprocess.check_output('docker ps', shell=True)
            if s.find(container_name) != -1:
                break
        self._inform_load_balancer(container_name, port)
        settings.current_available_port += 1

    def start_threads(self):
        self.listener.start()

    @staticmethod
    def _run_docker_container(container_name, port, image_name):
        """
        Helper function that builds and runs a new docker container at the 
        specified port using the specified image.
        """
        print "\n"
        print "="*80
        print "Creating a new docker container..."
        supervisor._kill_and_remove_docker_container(container_name)
        print "Running docker container {0} from image {1}".format(
            container_name, image_name)
        p = subprocess.Popen('docker build -t wikipedia_pbg4930 -f '
                             'Dockerfile_WikipediaApp .',
                             cwd=os.path.dirname(os.path.realpath(os.getcwd())),
                             shell=True)
        if p.wait() != 0:
            print "Could not build container! "
        p = subprocess.Popen('docker run --name {0} --link '
                             'pbg4930_psql:psql -e PYTHONUNBUFFERED=0 -e port={1} -e '
                             'server_address={3} -d -p {1}:{1} {2}'.format(
            container_name, port, image_name, hostname),
                             cwd=os.path.dirname(os.path.realpath(os.getcwd())),
                             shell=True)
        if p.wait() != 0:
            print "Could not run container! "
        print "=" * 80

    @staticmethod
    def _kill_and_remove_docker_container(container_name):
        """
        Helper function to kill and remove docker containers from the 
        previous run.
        """
        for action in ('kill', 'rm'):
            p = subprocess.Popen(
                'docker {0} {1}'.format(action, container_name),
                cwd=os.path.dirname(os.path.realpath(os.getcwd())), shell=True,
                stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            if p.wait() != 0:
                print "Could not clean up any containers: " + container_name

    def _inform_load_balancer(self, container_name, port):
        """
        Helper function to inform the load balancer about the container that 
        was just created.
        """
        print "\nInforming load balancer about new container..."
        message = dict()
        message["host"] = hostname
        message["conatiner_name"] = container_name
        message["port"] = port
        self.make_request_to_url(
            self.current_load_balancer + "/information_from_supervisor",
            message)

    @staticmethod
    def make_request_to_url(url, message):
        """
        Helper function to make a request to a url.
        """
        data = urllib.urlencode(message)
        req = urllib2.Request("http://" + url + "?" + data)
        response = urllib2.urlopen(req)
        return response.read()

    def _setup_psql_container(self):
        """
        Called initially to setup the PostgreSQL database docker container.
        """
        print "Setting up psql container for other docker containers..."
        self._kill_and_remove_docker_container("pbg4930_psql")
        print "Killed and removed any psql containers."
        p = subprocess.Popen(
            ' docker run --name pbg4930_psql -e POSTGRES_USER=docker -e '
            'POSTGRES_PASSWORD=docker -e POSTGRES_DB=wikipedia -p 5432:5432 -d '
            'postgres', cwd=os.path.dirname(os.path.realpath(os.getcwd())),
            shell=True)
        if p.wait() != 0:
            print "Could not setup psql container! "


class SupervisorListener(threading.Thread):
    """
    Class/Thread to enable the supervisor to listen to incoming communication
    from the load balancer.
    """
    def __init__(self):
        threading.Thread.__init__(self)

    def run(self):
        server_address = (
        socket.gethostbyname(hostname), settings.supervisor_listen_port)
        socket_object.bind(server_address)
        while True:
            print "\n"
            print "="*80
            print "Listening at {0}".format(server_address)
            data, client_address = socket_object.recvfrom(1024)
            self.process_incoming_message(data, client_address)

    @staticmethod
    def process_incoming_message(data, sender):
        message = json.loads(data)
        print "Code: {0} received from {1}".format(message["code"], sender)
        print "Message: {0}".format(message)
        code = message["code"]
        supervisor.current_load_balancer = message["hostname"]
        if code == 0:
            supervisor.create_contatiner()


if __name__ == '__main__':
    global supervisor
    supervisor = Supervisor()
