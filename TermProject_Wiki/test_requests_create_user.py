"""
__author__ = "Palash Gandhi (pbg4930@rit.edu)"
__date__ = 04/28/2017
"""
import requests


def make_request_to_url(url, message=None):
    r = requests.post(url, message)
    return r


if __name__ == '__main__':
    for (username, password) in [("aa", "aa"), ("bb", "bb"), ("cc", "aa"),
                                 ("dd", "aa"), ("ee", "aa"), ("ff", "aa"),
                                 ("gg", "aa"), ("hh", "aa"), ("ii", "aa"),
                                 ("jj", "aa"), ("kk", "aa"), ("ll", "aa"),
                                 ("mm", "aa"), ("nn", "aa"), ("oo", "aa"),
                                 ("pp", "aa"), ("qq", "aa"), ("rr", "aa"),
                                 ("ss", "aa"), ("tt", "aa"), ("uu", "aa"),
                                 ("vv", "aa"), ("ww", "aa"), ("xx", "aa"),
                                 ("yy", "aa"), ("zz", "aa"), ("a", "aa"),
                                 ("b", "aa"), ("c", "aa"), ("d", "aa"),
                                 ("e", "aa"), ("f", "aa"), ("g", "aa"),
                                 ("h", "aa"), ("i", "aa"), ("j", "aa"),
                                 ("k", "aa"), ("l", "aa"), ("m", "aa"),
                                 ("n", "aa"), ("o", "aa"), ("p", "aa"),
                                 ("1", "aa"), ("12", "aa"), ("123", "aa"),
                                 ("2", "aa"), ("22", "aa"), ("223", "aa"),]:
        message = dict()
        message["username"] = username+"@"+username
        message["password"] = password
        message["role"] = 1
        r = make_request_to_url("http://yes.cs.rit.edu:55000/create_user",
                                message)
        print r.status_code
