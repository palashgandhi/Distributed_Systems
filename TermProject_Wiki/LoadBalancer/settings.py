physical_servers = ["domino.cs.rit.edu", "argus.cs.rit.edu", "cyclops.cs.rit.edu", "midas.cs.rit.edu",
                    "tiresias.cs.rit.edu", "carya.cs.rit.edu", "argonaut.cs.rit.edu"]
load_balancer_port = 55000
supervisor_port = 55001
create_new_container_threshold = 5
replicate_data_threshold = 10