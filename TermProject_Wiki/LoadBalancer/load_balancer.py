"""
__author__ = "Palash Gandhi (pbg4930@rit.edu)"
__date__ = 04/28/2017
"""

import os
import socket
import json
import threading
import traceback
import urllib
import urllib2
import time
import redis
import sys
import settings
from flask import Flask, render_template, request

'''
This file contains the logic for load balancer which acts as the entry point 
into the system. 
'''

app = Flask(__name__)
hostname = socket.getfqdn()
redis_db = redis.StrictRedis(host="db", port=6379, db=0)
global lb


@app.route('/')
def index():
    """
    When a user tries to access the system from the url, the user should 
    first login/create a new account.
    Returns: the login page (HTML).
    """
    return render_template("login.html")


@app.route('/home', methods=["GET"])
def home():
    """
    Flask route for the index page.
    """
    user = request.args.get('user')
    role = request.args.get('role')
    return render_template("index.html", user=user, role=role,
                           articles=json.loads(redis_db.get('articles')))


@app.route('/create_user', methods=["POST"])
def create_user():
    """
    Flask route to create a user. Called when user submits the create user 
    form from the login page. This method creates a new user from a selected 
    host:container combination.
    """
    print "\n"
    print "=" * 80
    user = request.form['username']
    password = request.form['password']
    role = request.form['role']
    try:
        (host, container) = get_host_for_user(user)
        record_request_to_host(host)
        print "Creating user at {0}:{1}".format(host, container)
        message = {'username': user, 'password': password, 'role': role}
        response = make_request_to_url(
            "http://{0}:{1}/create_user".format(host, container), message)
        if not redis_db.exists('articles'):
            redis_db.set('articles', json.dumps(dict()))
        articles = json.loads(redis_db.get('articles'))
        print "=" * 80
        print "\n\n"
        if response == "1":
            return render_template('index.html', user=user, role=role,
                                   articles=articles)
        else:
            return render_template('login.html', error=response)
    except:
        traceback.print_exc()


@app.route('/login_validate', methods=["POST"])
def login():
    """
    Flask route to validate a user login. Called when user submits the login 
    form from the login page. This method validates a username:password from a 
    selected host:container combination based on the hash of the username.
    """
    print "\n"
    print "=" * 80
    user = request.form['username']
    password = request.form['password']
    try:
        (host, container) = get_host_for_user(user)
        record_request_to_host(host)
        print "Validating login at {0}:{1}".format(host, container)
        response = make_request_to_url(
            "http://{0}:{1}/login_validate".format(host, container),
            {'username': user, 'password': password})
        print "=" * 80
        print "\n\n"
    except:
        traceback.print_exc()
    if not redis_db.exists('articles'):
        redis_db.set('articles', json.dumps(dict()))
    if response in ('0', '1', '2'):
        return render_template('index.html', user=user, role=response,
                               articles=json.loads(redis_db.get('articles')))
    else:
        return render_template('login.html', error=response)


def get_replica_if_applicable(physical_host):
    """
    Helper function that alternates between replicas of a physical host
    Args:
        physical_host: The hostname of the original host

    Returns: Any one replica picked in a round robin fashion.

    """
    replica_index = json.loads(redis_db.get('replicated_list'))[physical_host][
        "index"]
    replica_host = physical_host
    replcated_list = json.loads(redis_db.get('replicated_list'))
    if replica_index < len(replcated_list[physical_host]["hosts"]):
        replica_host = replcated_list[physical_host]["hosts"][replica_index]
        replcated_list[physical_host]["index"] += 1
    else:
        replcated_list[physical_host]["index"] = 0
    redis_db.set('replicated_list', json.dumps(replcated_list))
    return replica_host


def get_host_for_user(user):
    """
    Method that returns a physical host based on the hash value of the username.
    Args:
        user: The username 

    Returns: The physical host name.

    """
    containers = json.loads(redis_db.get('containers'))
    user_hash = get_string_hash(user) % (settings.physical_servers.__len__())
    if user_hash >= len(containers.keys()):
        user_hash = 0
    physical_host = containers.keys()[user_hash]
    replica_host = get_replica_if_applicable(physical_host)
    container = get_container_port_to_use(replica_host)
    print "For user: {0} using {1}:{2}".format(user, replica_host, container)
    return (replica_host, container)


@app.route('/write_new_article', methods=["GET"])
def write_new_article():
    """
    Flask route to direct user to the page to write an article.
    """
    user = request.args.get('user')
    role = request.args.get('role')
    return render_template('new_article.html', user=user, role=role)


@app.route('/new_article_submit', methods=["GET"])
def new_article_submit():
    """
    Flask route that is called when a user submits a new article to publish.
    """
    try:
        error = None
        user = request.args.get('user')
        role = request.args.get('role')
        title = request.args.get('title')
        article_text = request.args.get('article_text')
        if not redis_db.exists('articles'):
            redis_db.set('articles', json.dumps(dict()))
        articles = json.loads(redis_db.get('articles'))
        if title not in articles:
            success_in_writing = write_article_to_node(user, title,
                                                       article_text)
        else:
            error = "Title already in use. Please use another title."
        if error:
            success_in_writing = error
        return render_template("index.html", user=user, role=role,
                               success_in_writing=success_in_writing,
                               articles=json.loads(redis_db.get('articles')))
    except:
        traceback.print_exc()


def write_article_to_node(user, title, article_text):
    """
    Helper function to select a physical host to write an article to based on
    the hash of the title of the article and write the article to that node.
    Args:
        user: username
        title: Title of the article.
        article_text: Text for the article.

    Returns: Response from the selected host.
    """
    print "\n"
    print "=" * 80
    containers = json.loads(redis_db.get('containers'))
    title_hash = get_string_hash(title) % (settings.physical_servers.__len__())
    physical_host = containers.keys()[title_hash]
    replica = get_replica_if_applicable(physical_host)
    container = get_container_port_to_use(replica)
    record_request_to_host(replica)
    print "Writing article at {0}:{1}".format(replica, container)
    articles = json.loads(redis_db.get('articles'))
    articles[title] = replica
    redis_db.set('articles', json.dumps(articles))
    print "=" * 80
    print "\n\n"
    return _write_to_container(user, title, article_text,
                               "http://{0}:{1}/write_article".format(replica,
                                                                     container))


def _write_to_container(user, title, article_text, selected_container):
    """
    Helper function to build the message to be sent to a docker container to 
    write the article at the physical server.
    Args:
        user: username
        title: Title of the text
        article_text: Text of the article.
        selected_container: The hostname of the selected docker container.
    """
    message = dict()
    message["username"] = user
    message["title"] = title
    message["article_text"] = article_text
    return make_request_to_url(selected_container, message)


@app.route('/article', methods=["GET"])
def article_page():
    """
    Flask route to fetch the article text from the appropriate physical host. 
    """
    print "\n"
    print "="*80
    try:
        user = request.args.get('user')
        role = request.args.get('role')
        title = request.args.get('title')
        article_host = request.args.get('article_host')
        replica = get_replica_if_applicable(article_host)
        print "Getting article from replica: {0}. Original host: {1}".format(
            replica, article_host)
        record_request_to_host(replica)
        container = get_container_port_to_use(replica)
        message = dict()
        message["title"] = title
        article_details = json.loads(make_request_to_url(
            "http://{0}:{1}/get_article_text".format(replica, container),
            message))
        print "=" * 80
        return render_template('article.html', user=user, role=role,
                               article_host=article_host, title=title,
                               article_details=article_details)
    except:
        traceback.print_exc()


@app.route('/edit_article')
def edit_article():
    """
    Flask route to direct user to the page to edit an article.
    """
    try:
        user = request.args.get('user')
        role = request.args.get('role')
        title = request.args.get('title')
        article_host = request.args.get('article_host')
        article_text = request.args.get('article_text')
        return render_template('edit_article.html', user=user, role=role,
                               title=title, article_host=article_host,
                               article_text=article_text)
    except:
        traceback.print_exc()


@app.route('/edit_article_submit')
def edit_article_submit():
    """
    Flask route that is called when a user submits an update/edit to any 
    article.
    """
    print "\n"
    print "=" * 80
    try:
        user = request.args.get('user')
        role = request.args.get('role')
        title = request.args.get('title')
        article_host = request.args.get('article_host')
        replica_list = json.loads(redis_db.get('replicated_list'))
        for replica_host in replica_list[article_host]["hosts"]:
            container = get_container_port_to_use(replica_host)
            record_request_to_host(replica_host)
            print "\nEditing article at {0}:{1}".format(replica_host, container)
            article_text = request.args.get('article_text')
            message = dict()
            message["title"] = title
            message["article_text"] = article_text
            success_in_editing = make_request_to_url(
                "http://{0}:{1}/edit_article_submit".format(replica_host,
                                                            container), message)
            print "=" * 80
            print "\n\n"
        return render_template("index.html", user=user, role=role,
                               success_in_editing=success_in_editing,
                               articles=json.loads(redis_db.get('articles')))
    except:
        traceback.print_exc()


def get_container_port_to_use(host):
    """
    Helper function to choose a container from a physical host to request a 
    service to. This is done in a round robin manner for each host.
    """
    container_load_indices = json.loads(redis_db.get('container_load_indices'))
    containers = json.loads(redis_db.get('containers'))
    return containers[host][container_load_indices[host]]


def record_request_to_host(host):
    """
    Helper function that records any request that is sent to a host based on 
    which selections are made in a round robin manner.
    """
    container_load_indices = json.loads(redis_db.get('container_load_indices'))
    container_load_indices[host] = container_load_indices[host] + 1
    containers = json.loads(redis_db.get('containers'))
    if container_load_indices[host] >= len(containers[host]):
        container_load_indices[host] = 0
    redis_db.set('container_load_indices', json.dumps(container_load_indices))
    host_request_count = json.loads(redis_db.get('host_request_count'))
    host_request_count[host] = host_request_count[host] + 1
    if host_request_count[host] == settings.create_new_container_threshold:
        balance_load(host, host_request_count[host])
    elif host_request_count[host] == settings.replicate_data_threshold:
        host_request_count[host] = 0
        balance_load(host, host_request_count[host], replicate=True)
    redis_db.set('host_request_count', json.dumps(host_request_count))


def balance_load(host, request_count, replicate=False):
    """
    If the number of requests to a host are equal to the thresholds set in 
    the settings, appropriate actions are taken to either create a new 
    container at a physical host or to replicate data from this physical host
    to another.
    """
    print "For host {0} request count = {1}, " \
          "replicas= {2}".format(host, request_count,
                                 json.loads(redis_db.get('replicated_list'))[
                                     host]["hosts"])
    if not replicate:
        print "\n"
        print "=" * 80
        print "Asking {0} to create a new container.".format(host)
        lb.create_container(host)
        print "=" * 80
    if replicate:
        print "\n"
        print "=" * 80
        replicated_list = json.loads(redis_db.get('replicated_list'))
        replicated_hosts = replicated_list[host]["hosts"]
        max_index = 0
        for replica_host in replicated_hosts:
            if settings.physical_servers.index(replica_host) > max_index:
                max_index = settings.physical_servers.index(replica_host)
        max_index += 1
        if max_index >= settings.physical_servers.__len__():
            max_index = 0
        if settings.physical_servers[0] not in replicated_hosts:
            target = settings.physical_servers[max_index]
            print "\nReplcating from {0} to {1}".format(host, target)
            source_container = get_container_port_to_use(host)
            message = dict()
            message["replicate_to"] = target + ":" + get_container_port_to_use(
                target)
            response = make_request_to_url(
                "http://{0}:{1}/replicate_data".format(host, source_container),
                message)
            if response == "1":
                print "Replication successful."
                replicated_list[host]["hosts"].append(target)
                redis_db.set('replicated_list', json.dumps(replicated_list))
            else:
                print "Replication not successful."
        else:
            print "Replicated at all hosts. max_index={0}, replicated " \
                  "hosts={1}".format(max_index, replicated_hosts)


@app.route('/information_from_supervisor', methods=["POST", "GET"])
def process_info_from_supervisor():
    """
    Route that is called by the supervisor to inform the load balancer of th
    new container that it created.
    """
    print "\n"
    print "=" * 80
    try:
        data = request.args
        print "Information received from supervisor."
        print "Container created at {0} with name {1} at port {2}".format(
            data.get("host"), data.get("conatiner_name"), data.get("port"))
        exists = redis_db.exists('containers')
        if not exists:
            redis_db.set('containers', json.dumps(dict()))
        containers = json.loads(redis_db.get('containers'))
        if data.get("host") not in containers:
            containers[data.get("host")] = []
        containers[data.get("host")].append(data.get("port"))
        redis_db.set('containers', json.dumps(containers))
        print "=" * 80
        return "Ok"
    except:
        traceback.print_exc()


def make_request_to_url(url, message=None):
    """
    Helper function to make a request to a service/app.
    """
    if message:
        data = urllib.urlencode(message)
        req = urllib2.Request("{0}?{1}".format(url, data))
    else:
        req = urllib2.Request("{0}".format(url))
    response = urllib2.urlopen(req)
    return response.read()


def get_string_hash(text):
    """
    Helper function to get the string hash given a string.
    """
    return hash(text) + sys.maxsize + 1


class LoadBalancer():
    """
    Main class that starts the listener threads and the flask app.
    """
    SERVER_CONTAINERS = dict()
    start_node = None

    def __init__(self):
        pass

    def create_container(self, server):
        """
        Method that sends a message to the selected server asking it to 
        create a container.
        """
        message = dict()
        message["code"] = 0
        message["hostname"] = os.environ["hostname"] + ":" + os.environ["port"]
        self.send_message(server, settings.supervisor_port, json.dumps(message))

    def setup(self):
        """
        Initially called to setup the redis variables and to create 1 
        container at each available physical server.
        """
        if (self.SERVER_CONTAINERS.__len__() == 0):
            container_load_indices = dict()
            host_request_count = dict()
            replicated_list = dict()
            for server in settings.physical_servers:
                self.create_container(server)
                container_load_indices[server] = 0
                host_request_count[server] = 0
                replicated_list[server] = {"hosts": [server], "index": 0}
            redis_db.set('container_load_indices',
                         json.dumps(container_load_indices))
            redis_db.set('host_request_count', json.dumps(host_request_count))
            redis_db.set('replicated_list', json.dumps(replicated_list))
        for item in settings.physical_servers:
            self.SERVER_CONTAINERS[item] = 0
        lb_record_manager = LoadBalancerRecordManager()
        lb_record_manager.start()
        self.start_flask_app()

    def start_flask_app(self):
        """
        Helper function to start the flask app.
        """
        app.run(host="0.0.0.0", port=settings.load_balancer_port, threaded=True)

    def send_message(self, hostname, port, message):
        """
        Helper function to send a message to a hostname:port.
        """
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        server_address = (hostname, port)
        sock.sendto(message, server_address)


class LoadBalancerRecordManager(threading.Thread):
    """
    Class/Thread that resets the request counts for each host in redis.
    """
    def __init__(self):
        print "Starting Load Balancer Manager thread..."
        threading.Thread.__init__(self)

    def run(self):
        while True:
            self.reset_host_request_counts()
            time.sleep(60)

    def reset_host_request_counts(self):
        if redis_db.exists('host_request_count'):
            print "Clearing host request counts..."
            host_request_count = json.loads(redis_db.get('host_request_count'))
            for host in host_request_count:
                host_request_count[host] = 0
            redis_db.set('host_request_count', json.dumps(host_request_count))

    def send_heartbeat(self, host, port):
        print "Sending heartbeat to http://{0}:{1}/heartbeat".format(host, port)
        make_request_to_url("http://{0}:{1}/heartbeat".format(host, port))


if __name__ == '__main__':
    lb = LoadBalancer()
    lb.setup()
