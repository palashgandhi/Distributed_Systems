"""
__author__ = "Palash Gandhi (pbg4930@rit.edu)"
__date__ = 04/28/2017
"""
import requests


def make_request_to_url(url, message=None):
    r = requests.get(url, message)
    return r


if __name__ == '__main__':
    articles = {}
    articles["White-headed stilt"] = "The white-headed stilt (Himantopus" \
                                     " leucocephalus) is a bird in the family Recurvirostridae. It is " \
                                     "sometimes considered a subspecies of the black-winged stilt (H. " \
                                     "himantopus). This shorebird has been recorded from Malaysia, Japan, " \
                                     "the Philippines, Brunei, Christmas Island, Indonesia, Palau, " \
                                     "Papua New Guinea, Australia and New Zealand."
    articles["Christmas Island"] = "Christmas Island, officially the Territory" \
                                   "of Christmas Island, is an Australian " \
                                   "external" \
                                   " territory comprising the island of the same" \
                                   " name. Christmas Island is located in the Indian" \
                                   " Ocean, around 350 kilometres (220 mi) south of" \
                                   " Java and Sumatra and around 1,550 kilometres" \
                                   " (960 mi) north-west of the closest point on" \
                                   " the Australian mainland. It has an area of" \
                                   " 135 square kilometres (52 sq mi)."
    articles["Christmas Island"] = "Flying Fish Cove is the main settlement" \
                                   " of Australia's Christmas Island. Although" \
                                   " it was originally named after British" \
                                   " survey-ship Flying-Fish, many maps simply" \
                                   " label it The Settlement. It was the " \
                                   "first British settlement on the island, " \
                                   "established in 1888."
    articles["Recreational diving"] = "Recreational diving or sport diving is" \
                                      " diving for the purpose of leisure and" \
                                      " enjoyment, usually when using scuba" \
                                      " equipment. The term recreational diving" \
                                      " may also be used in contradistinction" \
                                      " to technical diving, a more demanding" \
                                      " aspect of recreational diving which" \
                                      " requires greater levels of training."
    for i in range(0,8):
        articles["TITLE_RANDOM_"+str(i)] = "{0}".format(i)*150
    for title in articles.keys():
        message = dict()
        message["user"] = "aa@aa"
        message["role"] = 1
        message["title"] = title
        message["article_text"] = articles[title]
        r = make_request_to_url(
            "http://yes.cs.rit.edu:55000/new_article_submit", message)
        print r.status_code
