#!/usr/bin/env bash
#gnome-terminal --geometry 68x17+0+0 --title="top:argus" -e "bash -c \"expect server_monitor.exp argus.cs.rit.edu; exec bash\"";
#gnome-terminal --geometry 68x17+0+350 --title="top:domino" -e "bash -c \"expect server_monitor.exp domino.cs.rit.edu; exec bash\"";
#gnome-terminal --geometry 68x17+0+700 --title="top:cyclops" -e "bash -c \"expect server_monitor.exp cyclops.cs.rit.edu; exec bash\"";
#gnome-terminal --geometry 68x17+600+0 --title="top:midas" -e "bash -c \"expect server_monitor.exp midas.cs.rit.edu; exec bash\"";
#gnome-terminal --geometry 68x17+600+350 --title="top:tiresias" -e "bash -c \"expect server_monitor.exp tiresias.cs.rit.edu; exec bash\"";
#gnome-terminal --geometry 68x17+600+700 --title="top:carya" -e "bash -c \"expect server_monitor.exp carya.cs.rit.edu; exec bash\"";
#gnome-terminal --geometry 68x17+1200+0 --title="top:argonaut" -e "bash -c \"expect server_monitor.exp argonaut.cs.rit.edu; exec bash\"";
#sleep 15;
#gnome-terminal \
#--tab --title="Transfer files" -e "bash -c \"expect transfer_src.exp;exec bash\"";
#sleep 8;
gnome-terminal \
--tab --title="Redis:build_and_run" -e "bash -c \"expect build_and_run_redis.exp yes.cs.rit.edu; exec bash\"";
sleep 5;
gnome-terminal \
--tab --title="supervior:argus" -e "bash -c \"expect run_supervisor.exp argus.cs.rit.edu; exec bash\"" \
--tab --title="supervior:domino" -e "bash -c \"expect run_supervisor.exp domino.cs.rit.edu; exec bash\"" \
--tab --title="supervior:cyclops" -e "bash -c \"expect run_supervisor.exp cyclops.cs.rit.edu; exec bash\"" \
--tab --title="supervior:midas" -e "bash -c \"expect run_supervisor.exp midas.cs.rit.edu; exec bash\"" \
--tab --title="supervior:tiresias" -e "bash -c \"expect run_supervisor.exp tiresias.cs.rit.edu; exec bash\"" \
--tab --title="supervior:carya" -e "bash -c \"expect run_supervisor.exp carya.cs.rit.edu; exec bash\"" \
--tab --title="supervior:argonaut" -e "bash -c \"expect run_supervisor.exp argonaut.cs.rit.edu; exec bash\"";
sleep 15
gnome-terminal --tab --title="loadbalancer:yes" -e "bash -c \"expect run_load_balancer.exp yes.cs.rit.edu; exec bash\"";
