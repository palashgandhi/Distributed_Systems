gnome-terminal \
--tab --title="terminal:argus" -e "bash -c \"expect open_terminals.exp argus.cs.rit.edu; exec bash\"" \
--tab --title="terminal:domino" -e "bash -c \"expect open_terminals.exp domino.cs.rit.edu; exec bash\"" \
--tab --title="terminal:cyclops" -e "bash -c \"expect open_terminals.exp cyclops.cs.rit.edu; exec bash\"" \
--tab --title="terminal:midas" -e "bash -c \"expect open_terminals.exp midas.cs.rit.edu; exec bash\"" \
--tab --title="terminal:tiresias" -e "bash -c \"expect open_terminals.exp tiresias.cs.rit.edu; exec bash\"" \
--tab --title="terminal:carya" -e "bash -c \"expect open_terminals.exp carya.cs.rit.edu; exec bash\"" \
--tab --title="terminal:argonaut" -e "bash -c \"expect open_terminals.exp argonaut.cs.rit.edu; exec bash\"";