import java.util.HashMap;

/**
 * @author Palash Gandhi (pbg4930)
 * @date 28/2/17 12:25 AM
 * Distributed_Systems
 */
public class Transaction {

    /**
     * Main class that initializes and starts the threads for the service.
     * Run instructions: java Transaction <port_for_this_node> <hostname1>
     * <port_for_hostname1> <hostname2> <port_for_hostname2>
     */

    int port;
    HashMap<String, Integer> hosts = new HashMap<>();
    TransactionHandler handler;
    TransactionListener listener;
    SnapshotManager smgr;

    Transaction(String[] args) {
        /*
        * Constructor for class.
        * */
        process_input(args);
        smgr = new SnapshotManager(hosts);
        handler = new TransactionHandler(hosts, smgr);
        listener = new TransactionListener(port, handler);
    }

    public static void main(String[] args) {
        Transaction transaction = new Transaction(args);
        transaction.start_service();
    }

    public void start_service() {
        /*
        * Starts the SnapshotManager, listener and handler threads.
        * */
        System.out.println("Starting service...");
        smgr.start();
        listener.start();
        handler.start();
    }

    private void process_input(String[] input) {
        /*
        * Helper function to parse the command line arguments.
        * */
        if (input.length != 5) {
            System.out.println("Invalid usage. Usage: java Transaction " +
                    "<port> <hostname1> <port> <hostname2> <port>");
        }
        port = Integer.parseInt(input[0]);
        for (int i = 1; i < 4; i += 2) {
            hosts.put(input[i], Integer.parseInt(input[i + 1]));
        }
    }
}
