
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author Palash Gandhi (pbg4930)
 * @date 1/3/17 8:27 PM
 * Distributed_Systems
 */
public class SnapshotManager extends Thread {
    /**
     * SnapshotManager class that manages the snapshot at a node.
     * */
    HashMap<String, Integer> hosts;
    int snapshot_balance;
    int current_balance;
    public HashMap<String, Boolean> is_recording = new HashMap<>();
    public HashMap<String, ArrayList> incoming = new HashMap<>();
    String orig_makrker_sender;
    String hostname;
    TransactionHandler handler;

    SnapshotManager(HashMap<String, Integer> hosts) {
        this.hosts = hosts;
        try {
            hostname = InetAddress.getLocalHost().getHostName() + ".cs" +
                    ".rit.edu";
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }

    public void run() {
        /*
        * Starts a thread that initiates a snapshot ever 2 seconds at process 1.
        * */
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        while (true) {
            try {
                Thread.sleep(2000);
                if(hostname.equals("glados.cs.rit.edu")){
                    initiate_snapshot();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void initiate_snapshot() {
        /*
        * Method that starts the snapshot process at process 1.
        * */
        System.out.println("\t\tInitiating snapshot...");
        for (String host : hosts.keySet()) {
            snapshot_balance = current_balance;
            is_recording.put(host, true);
            send_message(host, -1);
            System.out.println("\t\tSending marker to " + host);
        }
    }

    private void record_and_forward_snapshot(String sender) {
        /*
        * When a process receives a marker for the first time, the process
        * starts recording incoming messages and its own state and forwards
        * the marker to all the other outgoing channels except to the original
        * sender.
        * */
        orig_makrker_sender = sender;
        snapshot_balance = current_balance;
        System.out.println("\t\tRecording and forwarding snapshot...");
        for (String host : hosts.keySet()) {
            if (!host.equals(sender)) {
                is_recording.put(host, true);
                System.out.println("\t\tForwarding marker to " + host);
                send_message(host, -1);
            }
        }
    }

    public void process_marker(String sender) {
        /*
        * When a process receives a marker from a node, this method is
        * invoked and checks if the marker needs to be sent back.
        * */
        System.out.println("\t\tReceived marker from "+sender);
        if (is_recording.containsKey(sender)) {
            if(is_recording.get(sender)){
                is_recording.put(sender, false);
                int count_recording = 0;
                for (String host : is_recording.keySet()) {
                    if (is_recording.get(host)) {
                        count_recording += 1;
                    }
                }
                if (count_recording == 0) {
                    if(!hostname.equals("glados.cs.rit.edu")) {
                        System.out.println("\t\tResending marker back to " +
                                ""+orig_makrker_sender);
                        send_message(orig_makrker_sender, -1);
                    }
                    print_snapshot();
                }
            }
            else{
                record_and_forward_snapshot(sender);
            }

        } else if (!is_recording.containsKey(sender)) {
            record_and_forward_snapshot(sender);
        }
    }

    public void process_message(int amount, String sender){
        /*
        * Used to record incoming messages.
        * */
        if (is_recording.get(sender)) {
            ArrayList<Integer> recorded;
            if (incoming.containsKey(sender)) {
                recorded = incoming.get(sender);
            } else {
                recorded = new ArrayList<>();
            }
            recorded.add(amount);
            incoming.put(sender, recorded);
        }
    }

    private void send_message(String host, int amount) {
        /*
        * Used to send markers to other nodes.
        * */
        int port = hosts.get(host);
        try {
            Socket socket = new Socket(host, port);
            OutputStream os = socket.getOutputStream();
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            DataOutputStream dos = new DataOutputStream(bos);
            dos.writeInt(amount);
            dos.writeInt(hostname.length());
            dos.write(hostname.getBytes());
            os.write(bos.toByteArray());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void print_snapshot() {
        System.out.println("=================================================");
        System.out.println("Snapshot balance: " + snapshot_balance);
        System.out.println("Channel states: " + incoming.toString());
        System.out.println("=================================================");
    }
}
