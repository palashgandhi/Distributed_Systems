

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Random;

/**
 * @author Palash Gandhi (pbg4930)
 * @date 1/3/17 4:42 PM
 * Distributed_Systems
 */
public class TransactionHandler extends Thread {

    /*
    * This class is used to perform transactions every 2 seconds with
    * randomly chosen hosts.
    * */

    String hostname;
    HashMap<String, Integer> hosts;
    String[] hosts_array;
    int balance = 1000;
    SnapshotManager snapshotManager;

    TransactionHandler(HashMap<String, Integer> hosts, SnapshotManager smgr) {
        /*
        * Constructor for this class.
        * @param hosts A hashmap of the pair host:port.
        * @param smgr Instance of SnapshotManager which manages the snapshot
        * at this node.
        * */
        this.hosts = hosts;
        snapshotManager = smgr;
        hosts_array = hosts.keySet().toArray(new String[hosts.size()]);
        snapshotManager.current_balance = balance;
        try {
            hostname = InetAddress.getLocalHost().getHostName()+".cs.rit.edu";
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }

    public void run() {
        for (String host : hosts_array) {
            snapshotManager.is_recording.put(host, false);
        }
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        begin_transactions();
    }

    private void begin_transactions() {
        /*
        * Perform transactions every 1 second until user stops execution.
        * */
        while (true) {
            Random rand = new Random();
            int choice_of_host = rand.nextInt(hosts.size());
            int amount = rand.nextInt(100);
            String host_to_connect = hosts_array[choice_of_host];
            int port_of_host = hosts.get(host_to_connect);
            System.out.println("\n\nConnecting to " + host_to_connect + ":" +
                    port_of_host + " for transferring $" + amount);
            send_message(host_to_connect, amount);
            balance -= amount;
            snapshotManager.current_balance = balance;
            print_balance();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void send_message(String host, int amount) {
        /*
        * Method that sends a transfer message request to the given host for
        * the given amount.
        * @param host The hostname of the node to transfer the amount to.
        * @amount The amount to transfer.
        * */
        int port = hosts.get(host);
        try {
            Socket socket = new Socket(host, port);
            OutputStream os = socket.getOutputStream();
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            DataOutputStream dos = new DataOutputStream(bos);
            dos.writeInt(amount);
            dos.writeInt(hostname.length());
            dos.write(hostname.getBytes());
            os.write(bos.toByteArray());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void process_message(int amount, String sender) {
        /*
        * Process an incoming message from a node for transfer request.
        * @param amount The amount received
        * @param sender The hostname of the node that sent the amount.
        * */
        if (amount == -1) {
            snapshotManager.process_marker(sender);
        } else {
            print_balance();
            System.out.println("Received transfer from " + sender + " for " +
                    "$" + amount+"\n");
            balance += amount;
            snapshotManager.current_balance = balance;
            print_balance();
            snapshotManager.process_message(amount, sender);
        }
    }

    public void print_balance() {
        System.out.println("Balance: " + balance);
    }

}
