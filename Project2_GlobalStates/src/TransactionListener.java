
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @author Palash Gandhi (pbg4930)
 * @date 1/3/17 4:30 PM
 * Distributed_Systems
 */
public class TransactionListener extends Thread{

    ServerSocket server_socket;
    int port;
    TransactionHandler handler;

    TransactionListener(int port_to_listen, TransactionHandler handler){
        /*
        * Constructor for class TransactionListener
        * */
        System.out.println("Initializing...");
        port = port_to_listen;
        this.handler = handler;
        establish_connections();
    }

    public void run(){
        listen();
    }

    public void establish_connections(){
        /*
        * Initialises a server socket at a port to receive messages.
        * */
        try {
            server_socket = new ServerSocket(port);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void listen(){
        /*
        * Receives messages and processes them accordingly.
        * */
        while(true){
            try {
                System.out.println("\nServer socket listening at port: "+port);
                Socket client = server_socket.accept();
                InputStream in = client.getInputStream();
                OutputStream os = client.getOutputStream();
                byte[] message = new byte[1024];
                in.read(message);
                ByteArrayInputStream bin = new ByteArrayInputStream(message);
                DataInputStream din = new DataInputStream(bin);
                int amount = din.readInt();
                int sender_len = din.readInt();
                byte[] sender_arr = new byte[sender_len];
                din.read(sender_arr);
                String sender = new String(sender_arr);
                handler.process_message(amount, sender);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
