import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;

/**
 * @author Palash Gandhi (pbg4930)
 * @date 15/3/17 11:57 PM
 * Distributed_Systems
 */
public class ProcessManager extends Thread {
    /**
     * Process Manager that tries to enter the CS and exits and sends the
     * token to another node if applicable.
     * */

    static SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");

    public void run() {
        if (Process.num_of_times_to_execute == -1) {
            while (true) {
                execute();
            }
        } else {
            for (int count = 0; count < Process.num_of_times_to_execute;
                 count++) {
                execute();
            }
        }
    }

    void execute() {
        /*
        * Helper function that executes and makes entry requests when possible.
        * Adds self to queue if the current node is not already in CS and is
        * not already enqueued.
        * */
        try {
            sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (!Process.request_queue.contains(Process.my_hostname) && !Process
                .currently_in_critical_section) {
            System.out.println(sdf.format(new java.util.Date()) + " PM: " +
                    "Adding self to queue...");
            Process.request_queue.offer(Process.my_hostname);
        }
        if (!Process.requested && !Process.currently_in_critical_section) {
            request_entry_to_critical_section();
        }
    }

    void request_entry_to_critical_section() {
        /*
        * Method that requests entry to CS. Checks if current process is the
        * token holder and accordingly assigns the CS to the oldest requester
         * in the queue.
        * */
        if (Process.holder.equals(Process.my_hostname) && Process
                .request_queue.isEmpty()) {
            enter_critical_section();
            Process.requested = false;
        } else {
            if (!Process.requested) {
                System.out.println("\n");
                System.out.println(sdf.format(new java.util.Date()) + " PM: " +
                        "Trying to enter CS...");
                System.out.println(sdf.format(new java.util.Date()) + " PM: " +
                        "Sending request to " + Process.holder);
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                DataOutputStream dos = new DataOutputStream(bos);
                try {
                    dos.writeInt(0);
                    dos.writeInt(Process.my_hostname.length());
                    dos.write(Process.my_hostname.getBytes());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Process.ps.send(bos.toByteArray(), Process.holder);
                Process.requested = true;
                Process.print_queue_and_holder();
            }
        }
    }

    void enter_critical_section() {
        /*
        * Simulator function that simulates entry to the CS. Sleeps for some
        * time and exits the CS.
        * */
        Process.print_queue_and_holder();
        Process.currently_in_critical_section = true;
        System.out.println(sdf.format(new java.util.Date()) + " PM: Entered " +
                "CS. My hostname: " + Process
                .my_hostname);
        System.out.println(sdf.format(new java.util.Date()) + " PM: " +
                "\n\n\t\t\t\t\t\t***In CS.***\n\n");
        try {
            sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        exit_critical_section();
    }

    void exit_critical_section() {
        /*
        * Method that is called after the node has finished accessing the
        * critical resource.
        * */
        System.out.println(sdf.format(new java.util.Date()) + " PM: " +
                "\n\n\t\t\t\t\t\t***Exiting CS.***\n\n");
        Process.currently_in_critical_section = false;
        if (!Process.request_queue.isEmpty()) {
            String oldest_requester = Process.request_queue.poll();
            send_token(oldest_requester);
            Process.holder = oldest_requester;
            Process.requested = false;
            if (!Process.request_queue.isEmpty()) {
                Process.pl.send_request(Process.holder);
                Process.requested = true;
            }
        }
        Process.print_queue_and_holder();
    }

    void send_token(String host) {
        /*
        * Helper function that sends the token to the given host.
        * */
        System.out.println(sdf.format(new java.util.Date()) + " PM: Sending " +
                "token to " + host);
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            DataOutputStream dos = new DataOutputStream(bos);
            dos.writeInt(1);
            dos.writeInt(Process.my_hostname.length());
            dos.write(Process.my_hostname.getBytes());
            Process.ps.send(bos.toByteArray(), host);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Process.print_queue_and_holder();
    }
}
