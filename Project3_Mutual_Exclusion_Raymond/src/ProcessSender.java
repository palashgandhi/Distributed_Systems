import java.io.IOException;
import java.io.OutputStream;
import java.net.ConnectException;
import java.net.Socket;

/**
 * @author Palash Gandhi (pbg4930)
 * @date 15/3/17 11:57 PM
 * Distributed_Systems
 */
public class ProcessSender {

    /**
     * Class that is used to send data to different nodes in the network.
     */
    public void send(byte[] message, String host) {
        try {
            Socket sock = new Socket(host, Process.port);
            OutputStream os = sock.getOutputStream();
            os.write(message);
            os.flush();
            sock.close();
        } catch (ConnectException ce) {
            System.out.println("PS: Connection Exception while connecting to " +
                    "" + host + ". Retrying...");
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            send(message, host);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
