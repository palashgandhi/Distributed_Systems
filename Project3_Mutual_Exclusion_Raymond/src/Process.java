import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.Queue;

/**
 * @author Palash Gandhi (pbg4930)
 * @date 15/3/17 8:34 PM
 * Distributed_Systems
 */
public class Process {
    /**
     * Class that is used to start the process at this node which follows
     * Raymond's algorithm to access the Critical Section.
     * Command line arguments: Hostname of parent in the logical tree (own
     * hostname if root)
     */
    static int port;
    static Queue<String> request_queue;
    static String holder;
    static boolean requested = false;
    static boolean currently_in_critical_section = false;

    static ProcessSender ps;
    static ProcessListener pl;
    static ProcessManager pm;
    static String my_hostname;
    static int num_of_times_to_execute;


    Process(String holder_input, int num_of_times_to_execute_input) {
        /*
        * Constructor for class that initializes the thread objects for
        * sending, listening and managing the critical section.
        * */
        port = 5667;
        request_queue = new LinkedList<>();
        holder = holder_input;
        num_of_times_to_execute = num_of_times_to_execute_input;
        try {
            my_hostname = InetAddress.getLocalHost().getHostName() + ".cs.rit" +
                    ".edu";
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        ps = new ProcessSender();
        pl = new ProcessListener();
        pm = new ProcessManager();
    }

    static void print_queue_and_holder() {
        /*
        * Helper function to print the current request queue, holder, value
        * of boolean: requested, and if the process is currently in CS.
        * */
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        Date date = new java.util.Date();
        System.out.println(sdf.format(date));
        System.out.println
                ("=================================================");
        System.out.println("Request queue: " + request_queue.toString());
        System.out.println("Holder: " + holder);
        System.out.println("Requested: " + requested);
        System.out.println("Currently in CS?: " + Process
                .currently_in_critical_section);
        System.out.println
                ("=================================================\n");
    }

    public static void main(String[] args) {
        System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n");
        String num = "-1";
        if (args.length > 1) {
            num = args[1];
        }
        Process process = new Process(args[0], Integer.parseInt(num));
        process.start_listener();
        process.start_manager();
    }

    void start_listener() {
        /*
        * Starts the listener thread.
        * */
        pl.start();
    }

    void start_manager() {
        /*
        * Starts the manager thread.
        * */
        pm.start();
    }
}
