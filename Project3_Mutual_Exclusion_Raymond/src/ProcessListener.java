import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.SimpleDateFormat;

/**
 * @author Palash Gandhi (pbg4930)
 * @date 15/3/17 11:57 PM
 * Distributed_Systems
 */
public class ProcessListener extends Thread {
    /**
     * Listener thread that processes incoming messages and takes appropriate
     * actions, sends requests/token if applicable.
     */

    static SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
    ServerSocket serverSocket;

    ProcessListener() {
        /*
        * Constructor for ProcessListener. Starts a server socket at
        * specified port.
        * */
        try {
            serverSocket = new ServerSocket(Process.port);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void run() {
        while (true) {
            listen();
        }
    }

    void listen() {
        /*
        * Keep listening and process incoming messages.
        * */
        System.out.println(sdf.format(new java.util.Date()) + " PL: Listening" +
                " " +
                "at port " +
                Process.port);
        byte[] message = new byte[256];
        try {
            Socket client = serverSocket.accept();
            InputStream in = client.getInputStream();
            in.read(message);
            process_message(message, client.getInetAddress().getHostName());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void process_message(byte[] message, String sender) {
        /*
        * Process the received message. Checks if the received message is a
        * token or a request message.
        * */
        ByteArrayInputStream bin = new ByteArrayInputStream(message);
        DataInputStream din = new DataInputStream(bin);
        try {
            int request_code = din.readInt();
            System.out.println(sdf.format(new java.util.Date()) + " PL: " +
                    "Received request code: " +
                    "" + request_code +
                    " from " + sender);
            if (request_code == 0) {
                int requester_length = din.readInt();
                byte[] requester_arr = new byte[requester_length];
                din.read(requester_arr);
                String requester = new String(requester_arr);
                process_enter_request(requester);
            } else if (request_code == 1) {
                System.out.println(sdf.format(new java.util.Date()) + " PL: " +
                        "Token received.");
                if (Process.request_queue.size() > 0) {
                    String oldest_requester = Process.request_queue.poll();
                    if (oldest_requester.equals(Process
                            .my_hostname)) {
                        Process.holder = Process.my_hostname;
                        Process.requested = false;
                        Process.pm.enter_critical_section();
                    } else {
                        send_token(oldest_requester);
                        Process.holder = oldest_requester;
                        Process.requested = false;
                        if (!Process.request_queue.isEmpty()) {
                            send_request(Process.holder);
                            Process.requested = true;
                        }
                    }
                } else {

                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        Process.print_queue_and_holder();

    }

    void process_enter_request(String requester) {
        /*
        * If the message is an entry request from another node, then this
        * method processes this request. Various cases like if self is the
        * holder of the token, if self is in CS, if it already has requested
        * to some other node etc are taken into consideration.
        * */
        System.out.println("\n");
        System.out.println(sdf.format(new java.util.Date()) + " PL: " +
                requester + " requesting to enter CS.");
        if (Process.holder.equals(Process.my_hostname)) {
            if (!Process.currently_in_critical_section &&
                    Process.request_queue.isEmpty()) {
                System.out.println(sdf.format(new java.util.Date()) + " PL: " +
                        Process.my_hostname + " not in CS" +
                        ".");
                System.out.println(sdf.format(new java.util.Date()) + " PL: " +
                        "Setting new holder to requester " +
                        "" + requester);
                Process.holder = requester;
                Process.requested = false;
                send_token(requester);
            } else {
                if (!Process.holder.equals(requester)) {
                    System.out.println(sdf.format(new java.util.Date()) + " " +
                            "PL:" +
                            " Adding " + requester + " to queue...");
                    Process.request_queue.offer(requester);
                }
            }
        } else {
            if (!Process.holder.equals(requester)) {
                System.out.println(sdf.format(new java.util.Date()) + " PL: " +
                        "Self is not holder.");
                System.out.println(sdf.format(new java.util.Date()) + " PL: " +
                        "Adding " + requester + " to queue...");
                Process.request_queue.offer(requester);
                if (!Process.requested) {
                    Process.requested = true;
                    send_request(Process.holder);
                }
            }
        }
    }

    private void send_token(String host) {
        /*
        * Helper function to send the token to the given host.
        * */
        System.out.println(sdf.format(new java.util.Date()) + " PL: Sending " +
                "token to " + host);
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            DataOutputStream dos = new DataOutputStream(bos);
            dos.writeInt(1);
            Process.ps.send(bos.toByteArray(), host);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void send_request(String holder) {
        /*
        * Helper function to send a request to the current token holder.
        * */
        System.out.println(sdf.format(new java.util.Date()) + " PL: Sending " +
                "request to " + holder);
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            DataOutputStream dos = new DataOutputStream(bos);
            dos.writeInt(0);
            dos.writeInt(Process.my_hostname.length());
            dos.write(Process.my_hostname.getBytes());
            Process.ps.send(bos.toByteArray(), holder);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
