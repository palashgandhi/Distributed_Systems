import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.util.Date;

/**
 * @author Palash Gandhi (pbg4930)
 * @date 6/4/17 10:03 AM
 * Distributed_Systems
 */
public class ProcessListener extends Thread {

    ServerSocket serverSocket;
    byte[] heartbeat_response;
    byte[] election_response_answer_msg;
    boolean answer_message_received = false;
    boolean elected_message_received = false;

    ProcessListener() {
        /**
         * Constructor for ProcessListener. Starts a server socket at
         * specified port.
         * */
        try {
            serverSocket = new ServerSocket(Process.port);
        } catch (IOException e) {
            e.printStackTrace();
        }
        ByteArrayOutputStream bos_election = new ByteArrayOutputStream();
        ByteArrayOutputStream heartbeat_bos = new ByteArrayOutputStream();
        DataOutputStream dos_election = new DataOutputStream(bos_election);
        DataOutputStream heartbeat_dos = new DataOutputStream(heartbeat_bos);
        try {
            dos_election.writeInt(3);
            heartbeat_dos.writeInt(1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        election_response_answer_msg = bos_election.toByteArray();
        heartbeat_response = heartbeat_bos.toByteArray();
    }

    public void run() {
        while (true) {
            listen();
        }
    }

    void listen() {
        /**
         * Keep listening and process incoming messages.
         * */
        System.out.println("\t\t\t\t\t" + Process.sdf.format(new Date()) + ":" +
                " " + "PL: Listening at" + " port " + Process.port);
        byte[] message = new byte[256];
        DatagramPacket packet = new DatagramPacket(message, message.length);
        try {
            Process.receiving_socket.receive(packet);
            String received = new String(packet.getData(), 0, packet
                    .getLength());
            process_message(received.getBytes(), packet.getAddress()
                    .getHostName() + ".cs.rit.edu");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void process_message(byte[] message, String sender) {
        /**
         * Process the received message. Checks if the received message is a
         * token or a request message.
         * */
        ByteArrayInputStream bin = new ByteArrayInputStream(message);
        DataInputStream din = new DataInputStream(bin);
        try {
            Process.print_state();
            int request_code = din.readInt();
            System.out.println("\n\t\t\t\t\t" + Process.sdf.format(new Date()
            ) + ": " + "PL:" + " " + "\nReceived request code: " +
                    request_code + " from " + sender + "\n");
            if (request_code == 0) {
                System.out.println("\t\t\t\t\t" + Process.sdf.format(new Date
                        ()) + ":" + " " + "PL: Heartbeat message " +
                        "received. " + "Replying..");
                send(heartbeat_response, sender, 5002);
            } else if (request_code == 1) {
                System.out.println("\t\t\t\t\t" + Process.sdf.format(new Date
                        ()) + ": PL: Heartbeat response received");
                Process.ph.received_heartbeat_response = true;
            } else if (request_code == 2) {
                System.out.println("\t\t\t\t\t" + Process.sdf.format(new Date
                        ()) + "PL:" + "Election message received from " +
                        sender);
                System.out.println("\t" + Process.sdf.format(new Date()) +
                        ":" + " PL: " + "Replying to election message from "
                        + sender);
                send(Process.pem.answer_msg, sender, 5002);
                if (!Process.participating) {
                    Process.pem.start_election();
                }
            } else if (request_code == 3) {
                System.out.println("\t\t\t\t\t" + Process.sdf.format(new Date
                        ()) + ":" + " PL: " + "Received answer message from "
                        + sender);
                answer_message_received = true;
            } else if (request_code == 4) {
                System.out.println("\t\t\t\t\t" + Process.sdf.format(new Date
                        ()) + ":" + " PL: " + "Received elected information "
                        + "message" + " " + "from " + sender);
                elected_message_received = true;
                Process.elected_hostname = sender;
                Process.elected_process_id = get_process_id(Process
                        .elected_hostname);
            }
            Process.print_state();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    int get_process_id(String hostname) {
        /**
         * Helper function to return the process id given a hostname.
         * */
        int id = -1;
        System.out.println(hostname);
        for (int higher_proc : Process.processes_higher.keySet()) {
            if (Process.processes_higher.get(higher_proc).equals(hostname)) {
                id = higher_proc;
            }
        }
        return id;
    }

    void send(byte[] message, String host, int port) {
        /**
         * Helper function to send a message to the specified host and port.
         * */
        try {
            DatagramPacket packet = new DatagramPacket(message, message
                    .length, InetAddress.getByName(host), port);
            Process.sending_socket.send(packet);
        } catch (Exception e) {
            System.out.println(Process.sdf.format(new Date()) + ": " +
                    "\n\n\t\t\t\t\tException: " + e);
        }
    }

}
