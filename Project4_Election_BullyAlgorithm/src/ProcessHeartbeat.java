import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.util.Date;

/**
 * @author Palash Gandhi (pbg4930)
 * @date 4/4/17 1:38 PM
 * Distributed_Systems
 */
public class ProcessHeartbeat extends Thread {
    /**
     * Thread that is used for the heartbeat mechanism.
     */

    byte[] heartbeat;
    boolean received_heartbeat_response = false;

    ProcessHeartbeat() {
        /**
         * Constructor for class that initializes the heartbeat message that
         * is used for the heartbeat mechanism.
         * */
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        DataOutputStream dos = new DataOutputStream(bos);
        try {
            dos.writeInt(0);
        } catch (IOException e) {
            e.printStackTrace();
        }
        heartbeat = bos.toByteArray();
    }

    public void run() {
        while (true) {
            if (!Process.participating && Process.elected_hostname.length() >
                    0 && Process.elected_process_id >= 0 && Process
                    .elected_process_id != Process.process_id) {
                send_heartbeat_message();
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            received_heartbeat_response = false;
        }
    }

    private void send_heartbeat_message() {
        /**
         * This method sends the heartbeat message to the current elected
         * leader and if after the timeout, the process does not respond
         * back, then lets the ProcessElectionManager thread know that a
         * failure was detected.
         * */
        System.out.println("\n\t\t\t\t\t\t\t\t\t\t" + Process.sdf.format(new
                Date()) + ":" + " PH: Sending heartbeat message to " +
                Process.elected_hostname);
        try {
            DatagramPacket packet = new DatagramPacket(heartbeat, heartbeat
                    .length, InetAddress.getByName(Process.elected_hostname),
                    5002);
            Process.sending_socket.send(packet);
            Thread.sleep(Process.timeout);
            if (!received_heartbeat_response) {
                System.out.println("\t\t\t\t\t\t\t\t\t\t" + Process.sdf
                        .format(new Date()) + ": PH: Cannot reach " + Process
                        .elected_hostname);
                Process.pem.failure_detected();
            }
        } catch (Exception ce) {
            System.out.println("Exception: " + ce);
        }
    }
}
