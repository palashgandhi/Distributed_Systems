import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.HashMap;

/**
 * @author Palash Gandhi (pbg4930)
 * @date 6/4/17 9:58 AM
 * Distributed_Systems
 */
public class Process {

    public static boolean participating = false;
    static int process_id;
    static String process_hostname;
    static int timeout;
    static int port = 5002;
    static HashMap<Integer, String> processes_higher;
    static HashMap<Integer, String> processes_lower;
    static ProcessListener pl;
    static ProcessHeartbeat ph;
    static ProcessElectionManager pem;
    static int elected_process_id = -1;
    static String elected_hostname = "";
    static SimpleDateFormat sdf = new SimpleDateFormat("HH:MM:SS");

    static DatagramSocket sending_socket;
    static DatagramSocket receiving_socket;

    Process(String args[]) {
        /**
         * Constructor for class. Initializes the datagram sockets at ports
         * 5001 for sending and 5002 for receiving and parses the command
         * line arguments that are passed.
         * */
        try {
            process_hostname = InetAddress.getLocalHost().getHostName() + ""
                    + ".cs.rit.edu";
            sending_socket = new DatagramSocket(5001);
            receiving_socket = new DatagramSocket(5002);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (SocketException e) {
            e.printStackTrace();
        }
        parse_args(args);
    }

    public static void main(String[] args) {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (args.length < 3) {
            System.out.println("Invalid usage.");
            System.out.println("Usage: java Process <timeout> " +
                    "<self_process_id> <process_id0>:<process0_hostname> " +
                    "...." + " <process_idN>:<processN_hostname>");
            System.exit(2);
        }
        Process process = new Process(args);
        process.setup_process();
        process.start_process();
    }

    public static void print_state() {
        System.out.println("=================================================");
        System.out.println("Current Leader: " + elected_process_id);
        System.out.println("Elected hostname: " + elected_hostname);
        System.out.println("Participating: " + participating);
        System.out.println("=================================================");
    }

    public void setup_process() {
        /**
         * Initializes objects of the 3 required classes viz.
         * ProcessListener, ProcessHeartbeat, ProcessElectionManager.
         * */
        pl = new ProcessListener();
        ph = new ProcessHeartbeat();
        pem = new ProcessElectionManager();
    }

    private void start_process() {
        /**
         * Starts the 3 threads required to run the bully election algorithm.
         * */
        print_state();
        pl.start();
        ph.start();
        pem.start();
    }

    private void parse_args(String[] args) {
        /**
         * Helper function to parse the command line arguments.
         * */
        processes_higher = new HashMap<>();
        processes_lower = new HashMap<>();
        timeout = Integer.parseInt(args[0]);
        process_id = Integer.parseInt(args[1]);
        for (String host_details : args[2].split(",")) {
            String[] inp_array = host_details.split(":");
            int proc_id = Integer.parseInt(inp_array[0]);
            String hostname = inp_array[1];
            if (proc_id > process_id) {
                processes_higher.put(proc_id, hostname);
            } else if (proc_id < process_id) {
                processes_lower.put(proc_id, hostname);
            }
        }
    }
}
