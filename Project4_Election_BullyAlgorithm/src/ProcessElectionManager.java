import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.util.Date;

/**
 * @author Palash Gandhi (pbg4930)
 * @date 15/3/17 11:57 PM
 * Distributed_Systems
 */
public class ProcessElectionManager extends Thread {
    /**
     * Election Manager that holds elections or replies with messages to
     * other processes according to the bully algorithm.
     */

    byte[] election_msg;
    byte[] elected_msg;
    byte[] answer_msg;

    ProcessElectionManager() {
        /**
         * Constructor for class. Initializes the election message, elected
         * message or the co-ordinator message and the answer message for the
         * bully algorithm.
         * */
        ByteArrayOutputStream bos_election = new ByteArrayOutputStream();
        DataOutputStream dos_election = new DataOutputStream(bos_election);
        ByteArrayOutputStream bos_elected = new ByteArrayOutputStream();
        DataOutputStream dos_elected = new DataOutputStream(bos_elected);
        ByteArrayOutputStream bos_answer = new ByteArrayOutputStream();
        DataOutputStream dos_answer = new DataOutputStream(bos_answer);
        try {
            dos_election.writeInt(2);
            dos_answer.writeInt(3);
            dos_elected.writeInt(4);
        } catch (IOException e) {
            e.printStackTrace();
        }
        election_msg = bos_election.toByteArray();
        elected_msg = bos_elected.toByteArray();
        answer_msg = bos_answer.toByteArray();
    }

    public void run() {
        start_election();
    }

    void start_election() {
        /**
         * Function that starts the election by sending election messages to
         * processes with higher ids than it's own id.
         * */
        Process.print_state();
        System.out.println("\n" + Process.sdf.format(new Date()) + ": PEM: "
                + "Starting election...");
        Process.participating = true;
        for (int higher_proc_id : Process.processes_higher.keySet()) {
            if (!Process.pl.answer_message_received && higher_proc_id !=
                    Process.elected_process_id) {
                System.out.println(Process.sdf.format(new Date()) + ": PEM: "
                        + "Sending election message to " + Process
                        .processes_higher.get(higher_proc_id));
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                send(election_msg, Process.processes_higher.get
                        (higher_proc_id));
            }
        }
        System.out.println(Process.sdf.format(new Date()) + ": PEM: Waiting "
                + "for " + Process.timeout * 5);
        try {
            Thread.sleep(Process.timeout * 5);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Process.sdf.format(new Date()) + ": PEM: Timeout "
                + "occured. Answer received: " + Process.pl
                .answer_message_received + ", " + "elected received: " +
                Process.pl.elected_message_received);
        if (!Process.pl.answer_message_received && Process.participating) {
            System.out.println(Process.sdf.format(new Date()) + ": Electing "
                    + "self.");
            elect_self();
            stop_election();
        } else if (Process.pl.answer_message_received) {
            if (!Process.pl.elected_message_received && Process.participating) {
                wait_and_restart_if_not_received();
            }
            if (Process.pl.elected_message_received && Process.participating) {
                stop_election();
            }
        }

    }

    void wait_and_restart_if_not_received() {
        /**
         * If an answer message is received but no co-ordinator message is
         * received within the second timeout value, this method restarts
         * the election.
         * */
        Process.print_state();
        System.out.println(Process.sdf.format(new Date()) + ": PEM: Waiting "
                + "for elected information.");
        try {
            Thread.sleep(Process.timeout * 5);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (!Process.pl.elected_message_received) {
            System.out.println(Process.sdf.format(new Date()) + ": PEM: " +
                    "Restarting election.");
            stop_election();
            start_election();
        } else {
            stop_election();
        }
    }

    void elect_self() {
        /**
         * Method that elects self and broadcasts a co-ordinator message to
         * every process with id lower than its own.
         * */
        Process.print_state();
        Process.elected_process_id = Process.process_id;
        Process.elected_hostname = Process.process_hostname;
        for (int lower_proc_id : Process.processes_lower.keySet()) {
            send(elected_msg, Process.processes_lower.get(lower_proc_id));
        }
        Process.print_state();
    }

    void failure_detected() {
        /**
         * Called when the heartbeat mechanism detects a failure.
         * */
        Process.print_state();
        if (!Process.participating) {
            start_election();
        }
    }


    void stop_election() {
        /**
         * Stops the election process.
         * */
        Process.print_state();
        System.out.println(Process.sdf.format(new Date()) + ": Stopping " +
                "election.");
        Process.pl.answer_message_received = false;
        Process.participating = false;
        Process.pl.elected_message_received = false;
        Process.print_state();
    }

    void send(byte[] message, String host) {
        /**
         * Helper function to send a message to the specified host.
         * */
        try {
            DatagramPacket packet = new DatagramPacket(message, message
                    .length, InetAddress.getByName(host), 5002);
            Process.sending_socket.send(packet);
        } catch (Exception e) {
            System.out.println("Exception: " + e);
        }

    }
}
